module gozlocal

go 1.13

require (
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/rs/cors v1.7.0
	github.com/rs/zerolog v1.17.2
)

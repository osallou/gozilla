package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	elasticsearch "github.com/elastic/go-elasticsearch"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	gozilla "gitlab.inria.fr/osallou/gozilla-lib"

	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"
)

var goz gozilla.GozContext
var token string
var user gozilla.User

func TestMain(m *testing.M) {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") != "" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}
	os.Setenv("GOZ_MONGO_DB", "goztest")
	os.Setenv("GOZ_FERNET", "423423423")

	setup()

	code := m.Run()
	os.Exit(code)
}

func setup() {
	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	mongoClient.Database(config.Mongo.DB).Drop(ctx)

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		panic(storageErr)
	}

	amqpHandler, amqpErr := gozilla.NewAmqpHandler(config)
	if amqpErr != nil {
		panic(amqpErr)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Msgf("Error creating the client: %s", err)
		panic(err)
	}

	goz = gozilla.GozContext{
		Mongo:       mongoClient,
		Config:      config,
		Storage:     storage,
		AmqpHandler: amqpHandler,
		Elastic:     es,
	}

	user = gozilla.User{
		ID:      "test",
		APIKey:  "123456",
		Profile: gozilla.UserProfileAdmin,
	}
	tokenMsg := gozilla.TokenMsg{
		User:   user,
		Expire: 0,
	}
	tokenJSON, _ := json.Marshal(tokenMsg)
	tkn, tknErr := gozilla.FernetEncode(goz.Config, tokenJSON)
	if tknErr != nil {
		log.Error().Err(tknErr).Msg("failed to create token")
		os.Exit(1)
	}
	token = string(tkn)
	log.Debug().Msgf("Token %s", token)

}

func _router() *mux.Router {
	r := mux.NewRouter()
	setRoutes(r)
	return r
}

func getHTTPHandler(fn http.HandlerFunc, req *http.Request) *httptest.ResponseRecorder {
	r := _router()
	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
	})
	handler := c.Handler(r)

	rr := httptest.NewRecorder()

	loggedRouter := handlers.LoggingHandler(os.Stdout, handler)
	ctxHandler := gozMiddleware(goz, loggedRouter)
	ctxHandler.ServeHTTP(rr, req)

	return rr
}

func TestHomeHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := getHTTPHandler(HomeHandler, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	var resData map[string]interface{}
	errData := json.NewDecoder(rr.Body).Decode(&resData)
	if errData != nil {
		t.Errorf("Invalid response %+v", errData)
	}

	if resData["message"] != "ok" {
		jsonData, _ := json.Marshal(resData)
		t.Errorf("handler returned unexpected body: got %s want %s",
			jsonData, "ok")
	}
}

func TestListReposUnauthorizedHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/repos/"+user.ID, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := getHTTPHandler(RepoListHandler, req)

	if status := rr.Code; status != http.StatusUnauthorized {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusUnauthorized)
		return
	}
}

func TestListReposNotFoundHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/repos/"+user.ID, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	rr := getHTTPHandler(RepoListHandler, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
		return
	}
}

func TestCreateSubjectHandler(t *testing.T) {

	data := gozilla.Subject{
		ID:   "test",
		Type: gozilla.SubjectPersonal,
	}
	jsonData, _ := json.Marshal(data)

	req, err := http.NewRequest("POST", "/api/v1/repos", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	rr := getHTTPHandler(SubjectCreateHandler, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
		var resData map[string]string
		json.NewDecoder(rr.Body).Decode(&resData)
		log.Error().Str("err", resData["message"]).Msg("failed")
		return
	}

	var resData map[string]gozilla.Subject
	errData := json.NewDecoder(rr.Body).Decode(&resData)
	if errData != nil {
		t.Errorf("Invalid response %+v", errData)
		return
	}
	subject, ok := resData["subject"]
	if !ok {
		t.Errorf("no subject found in answer: %+v", errData)
		return
	}
	if subject.Owner != user.ID {
		t.Errorf("owner not correctly forced %+v", subject)
		return
	}
}

func TestListReposEmptyHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/repos/"+user.ID, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	rr := getHTTPHandler(RepoListHandler, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
		var resData map[string]string
		json.NewDecoder(rr.Body).Decode(&resData)
		log.Error().Str("err", resData["message"]).Msg("failed")
		return
	}

	type RepoList struct {
		Repos   []gozilla.Repo
		Subject string
	}
	var resData RepoList
	errData := json.NewDecoder(rr.Body).Decode(&resData)
	if errData != nil {
		t.Errorf("Invalid response %+v", errData)
		return
	}
	if len(resData.Repos) != 0 {
		t.Errorf("should not find any repo")
	}

}

func TestCreateRepotHandler(t *testing.T) {

	data := gozilla.Repo{
		ID:      "testrepo",
		Subject: "test",
	}
	jsonData, _ := json.Marshal(data)

	req, err := http.NewRequest("POST", "/api/v1/repos/test", bytes.NewBuffer(jsonData))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))

	rr := getHTTPHandler(SubjectCreateHandler, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusCreated)
		var resData map[string]string
		json.NewDecoder(rr.Body).Decode(&resData)
		log.Error().Str("err", resData["message"]).Msg("failed")
		return
	}

	type RepoCreate struct {
		Repo    gozilla.Repo
		Subject string
	}
	var resData RepoCreate
	errData := json.NewDecoder(rr.Body).Decode(&resData)
	if errData != nil {
		t.Errorf("Invalid response %+v", errData)
		return
	}

	if resData.Subject != "test" {
		t.Errorf("subjet not correctly forced %+v", resData)
		return
	}
}

/*
func TestListReposNotFoundHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/api/v1/repos/"+user.ID, nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := getHTTPHandler(RepoListHandler, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
		return
	}

	var resData map[string]interface{}
	errData := json.NewDecoder(rr.Body).Decode(&resData)
	if errData != nil {
		t.Errorf("Invalid response %+v", errData)
		return
	}

	repos := resData["repo"].([]Repo)
	if len(repos) != 0 {
		jsonData, _ := json.Marshal(resData)
		t.Errorf("handler returned unexpected body: got %s want %s",
			jsonData, "0 repo")
		return
	}
}
*/

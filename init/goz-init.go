package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	elasticsearch "github.com/elastic/go-elasticsearch"
	gozilla "gitlab.inria.fr/osallou/gozilla-lib"
	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"
)

func loadESSchema(goz gozilla.GozContext, index string) error {
	// curl -T schema.json -XPUT -H "Content-Type: application/json" http://goz-elasticsearch:9200/gozfile
	schemaFile := "schema.json"
	if os.Getenv("GOZ_ES_SCHEMA") != "" {
		schemaFile = os.Getenv("GOZ_ES_SCHEMA")
	}
	jsonData, schemaErr := ioutil.ReadFile(schemaFile)
	if schemaErr != nil {
		log.Error().Err(schemaErr).Str("schema", schemaFile).Msg("Failed to load schema")
		return schemaErr
	}
	client := &http.Client{}
	req, reqErr := http.NewRequest("PUT", fmt.Sprintf("%s/%s", goz.Config.Elastic[0], index), bytes.NewBuffer(jsonData))
	if reqErr != nil {
		log.Error().Err(reqErr).Msg("Request creation failed")
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("Index creation error")
		return err
	}
	defer resp.Body.Close()
	log.Info().Msgf("Index creation status code: %d", resp.StatusCode)
	return nil
}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	gozCtx := gozilla.GozContext{
		Mongo:   mongoClient,
		Config:  config,
		Elastic: es,
	}

	log.Info().Msg("Loading schema from GOZ_ES_SCHEMA, default = ./schema.json")
	// Create, if needed, elasticsearch schema on index
	if esErr := loadESSchema(gozCtx, "gozfile"); esErr != nil {
		os.Exit(1)
	}

	log.Warn().Msg("Init gozilla setup")
	infoMsg := `
		Mandatory:
		 * GOZ_USER: admin user identifier to create
		 * GOZ_USER_PASSWORD: admin password
		Optional:
		 * GOZ_USER_EMAIL: admin email
		 * GOZ_USER_CUSTOMER: user customer, default = "admins", will be created if not exists
		 * GOZ_CUSTOMER_PLAN: customer plan to use, default = "admin", will be create if not exists
	`
	log.Info().Msgf("User creation usage: %s", infoMsg)
	if os.Getenv("GOZ_USER") != "" {
		userID := os.Getenv("GOZ_USER")
		userPwd := os.Getenv("GOZ_USER_PASSWORD")
		if userPwd == "" {
			fmt.Println("GOZ_USER is defined but password is empty")
			os.Exit(1)
		}

		planID := "admin"
		if os.Getenv("GOZ_CUSTOMER_PLAN") != "" {
			planID = os.Getenv("GOZ_CUSTOMER_PLAN")
		}

		plan := gozilla.PlanInfo{
			ID:          planID,
			Description: "Plan for admins",
		}
		_, pErr := plan.Exists(gozCtx)
		if pErr != nil {
			log.Warn().Str("plan", planID).Msg("plan does not exits, creating it...")
			plan.Create(gozCtx)
		}

		customerID := "admins"
		if os.Getenv("GOZ_USER_CUSTOMER") != "" {
			customerID = os.Getenv("GOZ_USER_CUSTOMER")
		}

		customer := gozilla.Customer{
			ID: customerID,
		}
		_, cErr := customer.Exists(gozCtx)
		if cErr != nil {
			// Create it
			customer = gozilla.Customer{
				ID:          customerID,
				Name:        customerID,
				Description: "Customer for internal admins",
				Plan:        planID,
			}
			log.Warn().Str("customer", customer.ID).Msg("Customer does not exits, creating it...")
			customer.Create(gozCtx)
		}

		user := gozilla.User{
			ID:       userID,
			Password: userPwd,
			Customer: customerID,
			Profile:  gozilla.UserProfileAdmin,
			Email:    os.Getenv("GOZ_USER_EMAIL"),
		}
		_, userErr := user.Exists(gozCtx)
		if userErr != nil {
			err := user.Register(gozCtx)
			if err != nil {
				log.Error().Err(err).Str("user", userID).Msg("Failed to create user")
			} else {
				log.Info().Str("user", userID).Msg("User created")
			}
		} else {
			log.Info().Str("user", userID).Msg("User already exists, skipping...")
		}
	} else {
		log.Warn().Msg("No GOZ_USER found, skipping admin user creation")
	}
}

module goz-init

go 1.13

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/rs/zerolog v1.18.0
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200409153114-21f623060994
	go.mongodb.org/mongo-driver v1.3.2
)

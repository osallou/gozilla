package main

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"text/template"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/cors"
	"github.com/streadway/amqp"
	"golang.org/x/crypto/bcrypt"

	mongo "go.mongodb.org/mongo-driver/mongo"
	mongoOptions "go.mongodb.org/mongo-driver/mongo/options"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	b64 "encoding/base64"

	gozilla "gitlab.inria.fr/osallou/gozilla-lib"

	elasticsearch "github.com/elastic/go-elasticsearch"

	"github.com/weaveworks/common/middleware"
)

// Version of server
var Version string

const currentAPIversion = "v1.0"

type favContextKey string

var (
	// RequestDuration calculates requests duration for prometheus
	RequestDuration = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "request_duration_seconds",
		Help:    "Time (in seconds) spent serving HTTP requests.",
		Buckets: prometheus.DefBuckets,
	}, []string{"method", "route", "status_code", "ws"})
)

// HomeHandler manages base entrypoint
var HomeHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	resp := map[string]interface{}{
		"version":    Version,
		"message":    "ok",
		"storage":    goz.Config.Storage.Type,
		"apiVersion": currentAPIversion,
		"cas":        goz.Config.Auth.CAS,
		"url":        goz.Config.URL,
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

// NYIHandler fake handler for not yet implemented handlers
var NYIHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotImplemented)
	resp := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(resp)
}

// SubjectListHandler list all subjects user is owner or member
var SubjectListHandler = func(w http.ResponseWriter, r *http.Request) {

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subjectSearch := gozilla.Subject{}

	subjects, subjectsErr := subjectSearch.List(goz)

	if subjectsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": subjectsErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"subjects": subjects}
	json.NewEncoder(w).Encode(respError)
	return
}

// SubjectGetHandler returns subject info, quota and user role in subject
var SubjectGetHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	goz.User = claims

	subjectInput := vars["subject"]

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	// Can user read from this subject ?
	if !subject.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	quota, quotaErr := goz.Storage.GetQuota(goz, subject.ID)
	if quotaErr != nil {
		quota = -1
	}

	role, _ := subject.MemberOf(goz, claims)

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"quota": quota, "subject": subject, "role": role}
	json.NewEncoder(w).Encode(respError)
	return
}

// RepoListHandler list all subject repositories
var RepoListHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	goz.User = claims

	subjectInput := vars["subject"]

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	// Can user read from this subject ?
	if !subject.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	repos, reposErr := subject.ListRepo(goz)

	if reposErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": reposErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	quota, quotaErr := goz.Storage.GetQuota(goz, subject.ID)
	if quotaErr != nil {
		quota = -1
	}

	role, _ := subject.MemberOf(goz, claims)

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repos": repos, "subject": subject, "role": role, "quota": quota}
	json.NewEncoder(w).Encode(respError)
	return
}

// RepoDeleteHandler delete repository if has no package
var RepoDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := subject.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if profile != gozilla.ProfileRepoAdmin && profile != gozilla.ProfileAdmin {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not repo admin"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repo, repoErr := subject.GetRepo(goz, repoInput)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packs, _ := repo.ListPackages(goz)
	if packs != nil && len(packs) > 0 {
		packnames := make([]string, 0)
		for _, p := range packs {
			packnames = append(packnames, p.ID)
		}
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("cannot delete, repo has packages: %s", strings.Join(packnames, ","))}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repo.Delete(goz)

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repo, "subject": subjectInput, "message": "deleted"}
	json.NewEncoder(w).Encode(respError)
	return
}

// RepoGetHandler get subject repositorie info
var RepoGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	role, isMember := subject.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repo, repoErr := subject.GetRepo(goz, repoInput)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repo, "subject": subjectInput, "role": role}
	json.NewEncoder(w).Encode(respError)
	return
}

// PackageVersionListHandler TODO
var PackageVersionListHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)

	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Repo:    repoInput,
		Subject: subjectInput,
		ID:      packageInput,
	}

	pack, packErr := packSearch.Get(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package not found: %s, %s", repoInput, packErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	versions, vpackErr := pack.GetVersions(goz)

	if vpackErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("packageversion not found: %s", packageInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repoInput, "subject": subjectInput, "package": pack, "versions": versions}
	json.NewEncoder(w).Encode(respError)
	return
}

// PackageVersionDeleteHandler TODO
var PackageVersionDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)

	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Repo:    repoInput,
		Subject: subjectInput,
		ID:      packageInput,
	}

	pack, packErr := packSearch.Get(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vpackSearch := gozilla.PackageVersion{
		Repo:    repoInput,
		Subject: subjectInput,
		Package: packageInput,
		Version: version,
	}

	if version == "_latest" {
		lastVersion, lastErr := vpackSearch.Latest(goz)
		if lastErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
			respError := map[string]interface{}{"message": "could not find latest"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		vpackSearch.Version = lastVersion
	}

	vpack, vpackErr := vpackSearch.Get(goz)
	if vpackErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("packageversion not found: %s", packageInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	files, _ := vpack.Files(goz)

	nbFiles := len(files)
	if nbFiles > 0 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("package version contains %d files, you need to delete files first", nbFiles)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vpack.Delete(goz)

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repoInput, "subject": subjectInput, "package": pack, "version": vpack, "message": "deleted"}
	json.NewEncoder(w).Encode(respError)
	return
}

// PackageVersionGetHandler TODO
// Should propose to get result with or without file listing
var PackageVersionGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)

	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	role, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Repo:    repoInput,
		Subject: subjectInput,
		ID:      packageInput,
	}

	pack, packErr := packSearch.Get(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vpackSearch := gozilla.PackageVersion{
		Repo:    repoInput,
		Subject: subjectInput,
		Package: packageInput,
		Version: version,
	}

	if version == "_latest" {
		lastVersion, lastErr := vpackSearch.Latest(goz)
		if lastErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusNotFound)
			respError := map[string]interface{}{"message": "could not find latest"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		vpackSearch.Version = lastVersion
	}

	vpack, vpackErr := vpackSearch.Get(goz)
	if vpackErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("packageversion not found: %s", packageInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	files, _ := vpack.Files(goz)

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"repo": repoInput, "subject": subjectInput, "package": pack, "version": vpack, "files": files, "role": role}
	json.NewEncoder(w).Encode(resp)
	return
}

// PackageDeleteHandler deletes package if no created version
var PackageDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)

	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Repo:    repoInput,
		Subject: subjectInput,
		ID:      packageInput,
	}

	pack, packErr := packSearch.Get(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	versions, _ := pack.GetVersions(goz)
	if versions != nil && len(versions) > 0 {
		vnames := make([]string, 0)
		for _, v := range versions {
			vnames = append(vnames, v.Version)
		}
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("package has versions, delete versions first: %s", strings.Join(vnames, ","))}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pack.Delete(goz)

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repoInput, "subject": subjectInput, "package": pack, "message": "deleted"}
	json.NewEncoder(w).Encode(respError)
	return
}

// PackageGetHandler get subject repositorie info
var PackageGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)

	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	role, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Repo:    repoInput,
		Subject: subjectInput,
		ID:      packageInput,
	}

	pack, packErr := packSearch.Get(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package not found: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repoInput, "subject": subjectInput, "package": pack, "role": role}
	json.NewEncoder(w).Encode(respError)
	return
}

// PackageListHandler list all subject repository packages
var PackageListHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)

	goz.User = claims

	subjectInput := vars["subject"]
	repoInput := vars["repo"]

	repoSearch := gozilla.Repo{
		ID:      repoInput,
		Subject: subjectInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, isMember := repo.MemberOf(goz, claims)
	if repo.Visibility > gozilla.VisibilityPublic && claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	} else if repo.Visibility > gozilla.VisibilityProtected && !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("not allowed")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packages, packagesErr := repo.ListPackages(goz)

	if packagesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": packagesErr}
		json.NewEncoder(w).Encode(respError)
		return
	}
	ownerSearch := gozilla.User{
		ID: repo.Owner,
	}
	owner, ownerErr := ownerSearch.Exists(goz)
	var plan gozilla.PlanInfo
	if ownerErr == nil {
		plan, _ = owner.GetPlan(goz)
	}

	w.Header().Add("Content-Type", "application/json")
	respError := map[string]interface{}{"repo": repo, "subject": subjectInput, "packages": packages, "plan": plan}
	json.NewEncoder(w).Encode(respError)
	return
}

// UserInfoHandler gets user info
// logged user or user defined by query param user if admin
var UserInfoHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": claimsErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	user := claims
	keys, ok := r.URL.Query()["user"]
	if ok && len(keys[0]) == 1 {
		if keys[0] != "" && claims.IsAdmin() {
			// Get user info
			userSearch := gozilla.User{
				ID: keys[0],
			}
			var userErr error
			user, userErr = userSearch.Exists(goz)
			if userErr != nil {
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(http.StatusNotFound)
				respError := map[string]interface{}{"message": userErr}
				json.NewEncoder(w).Encode(respError)
				return
			}
		}
	}

	plan, _ := user.GetPlan(goz)

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"user": user, "plan": plan}
	json.NewEncoder(w).Encode(resp)

}

// UserPasswordHandler updates user password
var UserPasswordHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	type PasswordForm struct {
		Password string `json:"password"`
	}
	pwdForm := &PasswordForm{}
	err := json.NewDecoder(r.Body).Decode(pwdForm)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	hashedPassword, _ := bcrypt.GenerateFromPassword([]byte(pwdForm.Password), bcrypt.DefaultCost)
	claims.Password = string(hashedPassword)
	claims.Save(goz)
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"message": "password updated"}
	json.NewEncoder(w).Encode(resp)
}

// UserAPIKeyHandler regenerates user apikey
var UserAPIKeyHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	claims.APIKey = gozilla.Random(8)
	claims.Save(goz)
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"apikey": claims.APIKey}
	json.NewEncoder(w).Encode(resp)
}

// AuthBindHandler authentify user and sends back a token
// token can be used in Authorization header with Bearer type
var AuthBindHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	type LoginForm struct {
		Login    string `json:"login"`
		Password string `json:"password"`
	}
	loginForm := &LoginForm{}
	err := json.NewDecoder(r.Body).Decode(loginForm)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	// try user/apikey
	authType := 0
	user, userErr := gozilla.UserAuth(goz, loginForm.Login, loginForm.Password)
	if userErr != nil {
		// Try user/password
		authType = 1
		user, userErr = gozilla.UserBind(goz, loginForm.Login, loginForm.Password)

	}

	if userErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": "wrong credentials"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	tokenmsg := gozilla.TokenMsg{
		User: user,
	}
	msg, _ := json.Marshal(tokenmsg)
	token, _ := gozilla.FernetEncode(goz.Config, msg)

	/*
		userSubject := gozilla.Subject{
			ID:         user.ID,
			Owner:      user.ID,
			Type:       gozilla.SubjectPersonal,
			Visibility: gozilla.VisibilityPublic,
		}
		// If user subject does not exists yet, create it
		_, subjectErr := userSubject.Exists(goz)
		if subjectErr != nil {
			subjectErr = userSubject.Create(goz)
			if subjectErr != nil {
				log.Error().Str("subject", user.ID).Msgf("failed to create subject")
			}
		}
	*/

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"user": user.ID, "token": string(token), "auth": authType}
	json.NewEncoder(w).Encode(resp)

}

//PackageCreateHandler TODO
var PackageCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	packInput := &gozilla.Package{}
	err := json.NewDecoder(r.Body).Decode(packInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	packInput.Subject = subjectInput
	repoInput := vars["repo"]
	packInput.Repo = repoInput

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}

	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo doesn't exists: %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, claims)
	if !isMember || profile > gozilla.ProfileDev {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of subject or insufficient rights: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if profile > gozilla.ProfileDev {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed to create package"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packErr := packInput.Create(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to create package: %s", packErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, subjectInput, repoInput, packInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"package": packInput}
	json.NewEncoder(w).Encode(resp)
	return
}

//RepoCreateHandler TODO
var RepoCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	repoInput := &gozilla.Repo{}
	err := json.NewDecoder(r.Body).Decode(repoInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput.Subject = subjectInput

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}

	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org doesn't exists: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, repoErr := repoInput.Exists(goz)
	if repoErr == nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo already exists: %s", repoInput.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := subject.MemberOf(goz, claims)
	if !isMember || profile > gozilla.ProfileDev {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of subject or insufficient rights: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if repoInput.Visibility < subject.Visibility {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "Cannot set a visibility higher than subject one"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if !claims.IsAdmin() || repoInput.Owner == "" {
		repoInput.Owner = claims.ID
	}

	repoErr = repoInput.Create(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to create repo: %s", repoErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, subjectInput, repoInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"subject": subjectInput, "repo": repoInput}
	json.NewEncoder(w).Encode(resp)
	return
}

// PackageUpdateHandler TODO
var PackageUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	packInput := &gozilla.Package{}
	err := json.NewDecoder(r.Body).Decode(packInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	repoSearch := gozilla.Repo{
		Subject: vars["subject"],
		ID:      vars["repo"],
	}

	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo does not exists: %s", repoSearch.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, goz.User)
	if !isMember || (profile < gozilla.ProfileAdmin || profile > gozilla.ProfileDev) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "need repo dev rights"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		Subject: repo.Subject,
		Repo:    repo.ID,
		ID:      vars["package"],
	}
	pack, packErr := packSearch.Exists(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package does not exists: %s", packSearch.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if packInput.Description != "" {
		pack.Description = packInput.Description
	}

	packErr = pack.Save(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to update pack: %s", packInput.ID), "error": repoErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"package": pack}
	json.NewEncoder(w).Encode(resp)
	return
}

// PackageVersionUpdateHandler TODO
var PackageVersionUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	packInput := &gozilla.PackageVersion{}
	err := json.NewDecoder(r.Body).Decode(packInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	repoSearch := gozilla.Repo{
		Subject: vars["subject"],
		ID:      vars["repo"],
	}

	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo does not exists: %s", repoSearch.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, goz.User)
	if !isMember || (profile < gozilla.ProfileAdmin || profile > gozilla.ProfileDev) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "need repo dev rights"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.PackageVersion{
		Subject: repo.Subject,
		Repo:    repo.ID,
		Package: vars["package"],
		Version: vars["version"],
	}
	pack, packErr := packSearch.Exists(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("package version does not exists: %s", packSearch.Version)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if packInput.Description != "" {
		pack.Description = packInput.Description
	}

	packErr = pack.Save(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to update package version: %s", packInput.Version), "error": repoErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"version": pack}
	json.NewEncoder(w).Encode(resp)
	return
}

// RepoUpdateHandler TODO
var RepoUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	repoInput := &gozilla.Repo{}
	err := json.NewDecoder(r.Body).Decode(repoInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	repoUpdate := gozilla.Repo{
		Subject: vars["subject"],
		ID:      vars["repo"],
	}

	repo, repoErr := repoUpdate.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo does not exists: %s", repoUpdate.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, goz.User)
	if !isMember || (profile != gozilla.ProfileRepoAdmin && profile != gozilla.ProfileAdmin) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "need repo admin rights"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repo.HomePage = repoInput.HomePage
	repo.IssuesPage = repoInput.IssuesPage

	if repoInput.Description != "" {
		repo.Description = repoInput.Description
	}
	if repoInput.Members != nil {
		repo.Members = repoInput.Members
	}
	repo.Visibility = repoInput.Visibility

	if repoInput.Meta != nil {
		repo.Meta = repoInput.Meta
	}

	if repoInput.Owner != "" && (repo.Owner == claims.ID || claims.IsAdmin()) {
		repo.Owner = repoInput.Owner
	}

	repoErr = repo.Save(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to update repo: %s", repoInput.ID), "error": repoErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"repo": repo}
	json.NewEncoder(w).Encode(resp)
	return
}

//PackageVersionCreateHandler TODO
var PackageVersionCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	packageVersionInput := &gozilla.PackageVersion{}
	err := json.NewDecoder(r.Body).Decode(packageVersionInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	packageVersionInput.Subject = subjectInput
	packageVersionInput.Repo = repoInput
	packageVersionInput.Package = packageInput

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}

	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repo doesn't exists: %s/%s", subjectInput, repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, packErr := packageVersionInput.Exists(goz)
	if packErr == nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("version already exists: %s", packageVersionInput.Version)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, claims)
	if !isMember || profile > gozilla.ProfileDev {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of repo or insufficient rights: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packErr = packageVersionInput.Create(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to create repo: %s", repoErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, subjectInput, repoInput, packageInput, packageVersionInput.Version}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"subject": subjectInput, "repo": repoInput, "package": packageInput, "version": packageVersionInput}
	json.NewEncoder(w).Encode(resp)
	return
}

//SubjectCreateHandler TODO
var SubjectCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subjectInput := &gozilla.Subject{}
	err := json.NewDecoder(r.Body).Decode(subjectInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	// Avoid conflicts with API for proxy
	if subjectInput.ID == "dl" || subjectInput.ID == "api" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "id is a reserved name"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	subjectInput.Owner = claims.ID
	if subjectInput.Type == gozilla.SubjectOrg {
		subjectInput.Customer = claims.Customer
	} else {
		// Check user does not already have a perso subject
		userSubjects, userSubjectsErr := claims.OwnedSubjects(goz)
		if userSubjectsErr == nil {
			for _, userSubject := range userSubjects {
				if userSubject.Type == gozilla.SubjectPersonal {
					w.Header().Add("Content-Type", "application/json")
					w.WriteHeader(http.StatusForbidden)
					respError := map[string]interface{}{"message": "already have a personal subject"}
					json.NewEncoder(w).Encode(respError)
					return
				}
			}
		}
	}

	_, subjectErr := subjectInput.Exists(goz)
	if subjectErr == nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org already exists: %s", subjectInput.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	subjectErr = subjectInput.Create(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to create subject %s: %s", subjectInput.ID, subjectErr), "error": subjectErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	vars := mux.Vars(r)
	version := vars["apiversion"]
	remote := []string{goz.Config.URL, "api", version, subjectInput.ID}
	w.Header().Add("Location", strings.Join(remote, "/"))

	resp := map[string]interface{}{"subject": subjectInput}
	json.NewEncoder(w).Encode(resp)
	return
}

// SubjectDeleteHandler deletes subject, must be admin or owner. Owner cannot delete subject = owner user id (only admin)
var SubjectDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectUpdate := gozilla.Subject{
		ID: vars["subject"],
	}

	subject, subjectErr := subjectUpdate.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org does not exists: %s", subjectUpdate.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subject.Owner != goz.User.ID && !goz.User.IsAdmin() {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "only repo owner can delete it"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subject.ID == goz.User.ID && !goz.User.IsAdmin() {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "only admin can delete user personal subject"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	nbRepo, nbRepoErr := subject.ListRepo(goz)
	if nbRepoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "failed to get subject repos"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if len(nbRepo) > 0 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "subject contains repositories, delete them first"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	subject.Delete(goz)

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"message": "delete", "subject": subject.ID}
	json.NewEncoder(w).Encode(resp)
	return
}

// SubjectUpdateHandler TODO
var SubjectUpdateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subjectInput := &gozilla.Subject{}
	err := json.NewDecoder(r.Body).Decode(subjectInput)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to decode message"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)
	subjectUpdate := gozilla.Subject{
		ID: vars["subject"],
	}

	subject, subjectErr := subjectUpdate.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("user/org does not exists: %s", subjectUpdate.ID)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subject.ID == subject.Owner {
		subject.Type = gozilla.SubjectPersonal
	} else {
		subject.Type = gozilla.SubjectOrg
	}

	if subject.Owner != goz.User.ID {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "admin or owner only"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if subjectInput.Owner != "" && subjectInput.Owner != subject.Owner {
		log.Warn().Str("subject", subjectUpdate.ID).Msg("Changing ownership")
		newOwnerSearch := gozilla.User{
			ID: subjectInput.Owner,
		}
		newOwner, newOwnerErr := newOwnerSearch.Exists(goz)
		if newOwnerErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "new owner does not exists"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		if subject.Customer != newOwner.Customer {
			// different customer not allowed
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "cannot change owner with a different customer"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		subject.Owner = subjectInput.Owner
	}

	if subjectInput.Description != "" {
		subject.Description = subjectInput.Description
	}
	if subjectInput.Members != nil {
		subject.Members = subjectInput.Members
	}

	subjectErr = subject.Save(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": fmt.Sprintf("failed to update subject: %s", subjectInput.ID), "error": subjectErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"subject": subject}
	json.NewEncoder(w).Encode(resp)
	return
}

// PublishHandler TODO
var PublishHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", subjectInput, repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if profile > gozilla.ProfileDev {
		// Not allowed
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not enough rights"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packSearch := gozilla.Package{
		ID:      packageInput,
		Subject: subjectInput,
		Repo:    repoInput,
	}

	now := time.Now()
	ts := now.Unix()
	pack, packErr := packSearch.Exists(goz)

	vpackSearch := gozilla.PackageVersion{
		Package: packageInput,
		Subject: subjectInput,
		Repo:    repoInput,
		Version: version,
	}

	vpack, vpackErr := vpackSearch.Exists(goz)

	if packErr != nil || vpackErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "package version does not exists"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	type PublishMsg struct {
		Discard bool `json:"discard"`
	}

	publishMsg := &PublishMsg{}
	err := json.NewDecoder(r.Body).Decode(publishMsg)
	/*
		if err != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			respError := map[string]interface{}{"message": "failed to decode message"}
			json.NewEncoder(w).Encode(respError)
			return
		}
	*/
	if publishMsg.Discard {
		vpack.Published = false
		vpack.LastUpdated = ts
		log.Debug().Str("pack", vpack.Package).Str("version", version).Msg("unpublish package")
		packSaveErr := vpack.Save(goz)
		if packSaveErr != nil {
			log.Error().Err(packSaveErr).Msg("Failed to unpublish package")
		}

		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "package unpublished", "package": packageInput, "version": version}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if vpack.Published {
		log.Debug().Msg("republishing...")
		/*
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": "package already published"}
			json.NewEncoder(w).Encode(respError)
			return
		*/
	}

	body, _ := json.Marshal(vpack)
	err = goz.AmqpHandler.Ch.Publish(
		"goz",     // exchange
		"publish", // routing key
		false,     // mandatory
		false,     // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
	if err != nil {
		log.Error().Err(err).Msgf("failed to send message\n")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "failed to publish"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if pack.Type == gozilla.DebianPackage {
		err = goz.AmqpHandler.Ch.Publish(
			"goz",    // exchange
			"debian", // routing key
			false,    // mandatory
			false,    // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(body),
			})
		if err != nil {
			log.Error().Err(err).Msgf("failed to send message\n")
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			respError := map[string]interface{}{"message": "failed to send message for debian"}
			json.NewEncoder(w).Encode(respError)
			return
		}
	}

	if pack.Type == gozilla.RpmPackage {
		err = goz.AmqpHandler.Ch.Publish(
			"goz", // exchange
			"rpm", // routing key
			false, // mandatory
			false, // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(body),
			})
		if err != nil {
			log.Error().Err(err).Msgf("failed to send message\n")
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			respError := map[string]interface{}{"message": "failed to send message for rpm"}
			json.NewEncoder(w).Encode(respError)
			return
		}
	}

	/*
		// Already exists
		pack.LastUpdated = ts
		pack.Published = true
		log.Debug().Str("pack", pack.ID).Str("version", version).Msg("publishing package")

		packSaveErr := pack.Save(goz)

		if packSaveErr != nil {
			log.Error().Err(packSaveErr).Msg("Failed to publish package")
		}
	*/

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	respError := map[string]interface{}{"message": "package publishing in progress", "package": packageInput, "version": version}
	json.NewEncoder(w).Encode(respError)
	return
}

func getContent(w http.ResponseWriter, r *http.Request, goz gozilla.GozContext, claims gozilla.User, claimsErr error, f gozilla.FileObject, simpleRedirect bool) {
	repoSearch := gozilla.Repo{
		Subject: f.GetSubject(),
		ID:      f.GetRepo(),
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", f.GetSubject(), f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile := gozilla.ProfileAnonymous
	isMember := false
	profile, isMember = repo.MemberOf(goz, claims)
	if repo.Visibility != gozilla.VisibilityPublic {
		if claimsErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
			json.NewEncoder(w).Encode(respError)
			return
		}
		if repo.Visibility == gozilla.VisibilityProtected {
			// All connected users are accepted
			isMember = true
		} else {
			// Only repo members can read
			if !isMember {
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(http.StatusForbidden)
				respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", f.GetRepo())}
				json.NewEncoder(w).Encode(respError)
				return
			}

		}
		// Else is logged and member of repo, user is allowed to read

	}

	vpackSearch := gozilla.PackageVersion{
		Package: f.GetPackage(),
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
		Version: f.GetVersion(),
	}

	if f.GetVersion() == "$latest" {

		lastVersion, lastErr := vpackSearch.Latest(goz)
		if lastErr != nil {
			log.Debug().Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Str("package", f.GetPackage()).Msg("latest requested but no version found")
		} else {
			vpackSearch.Version = lastVersion
			f.SetVersion(lastVersion)
			f.SetName(strings.ReplaceAll(f.GetName(), "$latest", lastVersion))
		}
	}

	pack, packErr := vpackSearch.Exists(goz)
	if packErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if !pack.Published && (!isMember || (isMember && profile > gozilla.ProfileRepoAdmin)) {
		w.Header().Add("Content-Type", "application/json")

		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "package not published"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fh, fErr := goz.Storage.GetObjectURL(goz, f, 5)
	if fErr != nil {
		log.Error().Err(fErr).Str("file", f.GetPath()).Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Msg("failed to save object")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	keys, ok := r.URL.Query()["noredirect"]
	if ok && len(keys[0]) == 1 {
		w.Header().Add("Content-Type", "application/json")
		respError := map[string]interface{}{"location": fh}
		json.NewEncoder(w).Encode(respError)
		return
	}

	gozilla.StatDownload(goz, f.GetSubject(), f.GetRepo(), f.GetPackage(), f.GetPackageType())
	log.Debug().Str("url", fh).Msg("redirect for download")

	if simpleRedirect {
		http.Redirect(w, r, fh, 302)
	} else {
		w.Header().Add("Location", fh)
		w.WriteHeader(http.StatusTemporaryRedirect)
	}

}

func deleteContent(w http.ResponseWriter, r *http.Request, goz gozilla.GozContext, claims gozilla.User, f gozilla.FileObject, simpleRedirect bool) {
	repoSearch := gozilla.Repo{
		Subject: f.GetSubject(),
		ID:      f.GetRepo(),
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", f.GetSubject(), f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile, isMember := repo.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if profile > gozilla.ProfileDev {
		// Not allowed
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not enough rights"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vpackSearch := gozilla.PackageVersion{
		Package: f.GetPackage(),
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
		Version: f.GetVersion(),
	}

	packSearch := gozilla.Package{
		ID:      f.GetPackage(),
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
	}

	pack, packErr := packSearch.Exists(goz)
	vpack, vpackErr := vpackSearch.Exists(goz)
	if packErr != nil || vpackErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package or version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fh, fErr := goz.Storage.DeleteObject(goz, f, 5)
	if fErr != nil {
		log.Error().Err(fErr).Str("file", f.GetPath()).Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Msg("failed to save object")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var packSaveErr error
	var vpackSaveErr error

	pack.Versions = nil

	vpackSaveErr = vpack.Save(goz)

	if vpackSaveErr != nil {
		log.Error().Err(packSaveErr).Msg("Failed to save package")
	}

	f.UnIndex(goz)

	log.Debug().Str("url", fh).Msg("redirect for delete")

	if simpleRedirect {
		http.Redirect(w, r, fh, 302)
	} else {
		w.Header().Add("Location", fh)
		w.WriteHeader(http.StatusTemporaryRedirect)
	}

}

func checkPutContent(w http.ResponseWriter, r *http.Request, goz gozilla.GozContext, claims gozilla.User, f gozilla.FileObject) error {
	repoSearch := gozilla.Repo{
		Subject: f.GetSubject(),
		ID:      f.GetRepo(),
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", f.GetSubject(), f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return fmt.Errorf("repo not found")
	}

	profile, isMember := repo.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return fmt.Errorf("acl error")
	}

	if profile > gozilla.ProfileDev {
		// Not allowed
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not enough rights"}
		json.NewEncoder(w).Encode(respError)
		return fmt.Errorf("acl error")
	}

	vpackSearch := gozilla.PackageVersion{
		Package: f.GetPackage(),
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
		Version: f.GetVersion(),
	}

	packSearch := gozilla.Package{
		ID:      f.GetPackage(),
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
	}

	now := time.Now()
	ts := now.Unix()
	packExists := false
	vpackExists := false
	pack, packErr := packSearch.Exists(goz)
	vpack, vpackErr := vpackSearch.Exists(goz)
	if packErr != nil {
		pack = packSearch
		pack.Created = ts
	} else {
		packExists = true
		if pack.Type != f.GetType() {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": fmt.Sprintf("pushing file type %d to package type %d", pack.Type, f.GetType())}
			json.NewEncoder(w).Encode(respError)
			return fmt.Errorf("invalid file type for package")
		}
	}

	if vpackErr != nil {
		vpack = vpackSearch
		vpack.Created = ts
		vpack.LastUpdated = ts
		vpack.Meta = make(map[string]string)
		vpack.Meta["type"] = string(f.GetType())
	} else {
		vpack.LastUpdated = ts
		vpackExists = true
	}

	var packSaveErr error
	pack.Versions = nil
	if !packExists {
		pack.Type = f.GetType()
		packSaveErr = pack.Create(goz)
		if packSaveErr != nil {
			log.Error().Err(packSaveErr).Msg("Failed to create package")
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusInternalServerError)
			respError := map[string]interface{}{"message": "ooppps"}
			json.NewEncoder(w).Encode(respError)
			return fmt.Errorf("pack save error")
		}
	}

	var vpackSaveErr error
	if vpackExists {
		vpackSaveErr = vpack.Save(goz)
	} else {
		vpackSaveErr = vpack.Create(goz)
	}
	if vpackSaveErr != nil {
		log.Error().Err(packSaveErr).Msg("Failed to save package version")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return fmt.Errorf("vpack save error")
	}
	return nil
}

func saveFile(goz gozilla.GozContext, r *http.Request, w http.ResponseWriter, f gozilla.FileObject) (string, error) {
	fh, fErr := goz.Storage.SaveObject(goz, f, 5)
	if fErr != nil {
		log.Error().Err(fErr).Str("file", f.GetPath()).Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Msg("failed to save object")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return "", fmt.Errorf("failed to save file")
	}

	var size int64
	if length, ok := r.Header["Content-Length"]; ok {
		hsize, herr := strconv.Atoi(length[0])
		if herr == nil {
			size = int64(hsize)
		}

	}
	now := time.Now()
	ts := now.Unix()

	f.SetMeta(gozilla.FileInfo{
		LastUpdated: ts,
		Size:        size,
	})

	f.Index(goz)
	return fh, nil
}

func putContent(w http.ResponseWriter, r *http.Request, goz gozilla.GozContext, claims gozilla.User, f gozilla.FileObject, simpleRedirect bool) {
	err := checkPutContent(w, r, goz, claims, f)
	if err != nil {
		log.Error().Err(err).Msg("an error occured")
		return
	}

	fh, fErr := saveFile(goz, r, w, f)
	if fErr != nil {
		log.Error().Err(fErr).Msgf("failed to save file")
		return
	}

	gozilla.StatUpload(goz, f.GetSubject(), f.GetRepo(), f.GetPackage(), f.GetPackageType())

	pvu := gozilla.PackageVersionUpdate{
		Subject: f.GetSubject(),
		Repo:    f.GetRepo(),
		Package: f.GetPackage(),
		Version: f.GetVersion(),
	}
	pvu.New(goz)

	log.Debug().Str("url", fh).Msg("redirect for upload")

	if simpleRedirect {
		http.Redirect(w, r, fh, 302)
	} else {
		w.Header().Add("Location", fh)
		w.WriteHeader(http.StatusTemporaryRedirect)
	}

}

// DebianRepoGetHandler TODO
var DebianRepoGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	filePath := vars["path"]

	f := gozilla.NewDebianRepoFileObject(subjectInput, repoInput, packageInput, filePath)

	repoSearch := gozilla.Repo{
		Subject: f.GetSubject(),
		ID:      f.GetRepo(),
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", f.GetSubject(), f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return
	}

	profile := gozilla.ProfileAnonymous
	isMember := false
	profile, isMember = repo.MemberOf(goz, claims)
	if repo.Visibility != gozilla.VisibilityPublic {
		if claimsErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
			json.NewEncoder(w).Encode(respError)
			return
		}
		if repo.Visibility == gozilla.VisibilityProtected {
			// All connected users are accepted
			isMember = true
		} else {
			// Only repo members can read
			if !isMember {
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(http.StatusForbidden)
				respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", f.GetRepo())}
				json.NewEncoder(w).Encode(respError)
				return
			}

		}

	}

	if !isMember || (isMember && profile > gozilla.ProfileRepoAdmin) {
		w.Header().Add("Content-Type", "application/json")

		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "package not published"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fh, fErr := goz.Storage.GetObjectURL(goz, f, 5)
	if fErr != nil {
		log.Error().Err(fErr).Str("file", f.GetPath()).Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Msg("failed to save object")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Str("url", fh).Msg("redirect for download")

	w.Header().Add("Location", fh)
	w.WriteHeader(http.StatusTemporaryRedirect)

}

// DebianGetHandler TODO
var DebianGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	filePath := vars["path"]
	//filePath = "debs/" + filePath

	f := gozilla.NewDebianFileObject(subjectInput, repoInput, packageInput, version, filePath)

	getContent(w, r, goz, claims, claimsErr, f, false)
}

// DebianPutHandler TODO
var DebianPutHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var distribution string
	var component string
	var architectures string
	qdistribution, distribok := r.URL.Query()["distribution"]
	paramsOk := true
	if !distribok {
		if qdistribution, distribok = r.Header["X-GOZ-Debian-Distribution"]; !distribok {
			paramsOk = false
		} else {
			distribution = qdistribution[0]
		}

	} else {
		distribution = qdistribution[0]
	}
	qcomponent, compok := r.URL.Query()["component"]
	if !compok || len(qcomponent) != 1 {
		if qcomponent, compok = r.Header["X-GOZ-Debian-Component"]; !distribok {
			paramsOk = false
		} else {
			component = qcomponent[0]
		}
	} else {
		component = qcomponent[0]
	}
	qarchitectures, archsok := r.URL.Query()["architectures"]
	if !archsok || len(qarchitectures) != 1 {
		if qarchitectures, archsok = r.Header["X-GOZ-Debian-Architecture"]; !distribok {
			paramsOk = false
		} else {
			architectures = qarchitectures[0]
		}
	} else {
		architectures = qarchitectures[0]
	}
	if !paramsOk {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("missing distribution, component or architecture params")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	//filePath := "debs/" + vars["path"]
	filePath := vars["path"]

	f := gozilla.NewDebianFileObject(subjectInput, repoInput, packageInput, version, filePath)
	f.SetExtra("distribution", distribution)
	f.SetExtra("component", component)
	f.SetExtra("architectures", architectures)
	putContent(w, r, goz, claims, f, false)
}

// RpmRepoGetHandler TODO
var RpmRepoGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	filePath := vars["path"]

	f := gozilla.NewRpmRepoFileObject(subjectInput, repoInput, packageInput, filePath)

	repoSearch := gozilla.Repo{
		Subject: f.GetSubject(),
		ID:      f.GetRepo(),
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", f.GetSubject(), f.GetRepo())}
		json.NewEncoder(w).Encode(respError)
		return
	}

	//profile := gozilla.ProfileAnonymous
	isMember := false
	_, isMember = repo.MemberOf(goz, claims)
	if repo.Visibility != gozilla.VisibilityPublic {
		if claimsErr != nil || repo.Visibility == gozilla.VisibilityPrivate {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
			json.NewEncoder(w).Encode(respError)
			return
		}

		// Only repo members can read
		if !isMember {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusForbidden)
			respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", f.GetRepo())}
			json.NewEncoder(w).Encode(respError)
			return
		}

	}

	fh, fErr := goz.Storage.GetObjectURL(goz, f, 5)
	if fErr != nil {
		log.Error().Err(fErr).Str("file", f.GetPath()).Str("subject", f.GetSubject()).Str("repo", f.GetRepo()).Msg("failed to save object")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "ooppps"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Str("url", fh).Msg("redirect for download")

	w.Header().Add("Location", fh)
	w.WriteHeader(http.StatusTemporaryRedirect)

}

// RpmGetHandler TODO
var RpmGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	filePath := vars["path"]
	//filePath = "debs/" + filePath

	f := gozilla.NewRpmFileObject(subjectInput, repoInput, packageInput, version, filePath)

	getContent(w, r, goz, claims, claimsErr, f, false)
}

// RpmPutHandler TODO
var RpmPutHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var distribution string
	var architectures string
	qdistribution, distribok := r.URL.Query()["distribution"]
	paramsOk := true
	if !distribok {
		if qdistribution, distribok = r.Header["X-GOZ-Rpm-Distribution"]; !distribok {
			paramsOk = false
		} else {
			distribution = qdistribution[0]
		}

	} else {
		distribution = qdistribution[0]
	}

	qarchitectures, archsok := r.URL.Query()["architectures"]
	if !archsok || len(qarchitectures) != 1 {
		if qarchitectures, archsok = r.Header["X-GOZ-Rpm-Architecture"]; !distribok {
			paramsOk = false
		} else {
			architectures = qarchitectures[0]
		}
	} else {
		architectures = qarchitectures[0]
	}
	if !paramsOk {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("missing distribution, component or architecture params")}
		json.NewEncoder(w).Encode(respError)
		return
	}

	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	//filePath := "debs/" + vars["path"]
	filePath := vars["path"]

	f := gozilla.NewRpmFileObject(subjectInput, repoInput, packageInput, version, filePath)
	f.SetExtra("distribution", distribution)
	f.SetExtra("architectures", architectures)
	putContent(w, r, goz, claims, f, false)
}

// MavenPutHandler manage maven file upload
var MavenPutHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	filePath := vars["path"]
	mvnElts := strings.Split(filePath, "/")
	if len(mvnElts) < 4 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		respError := map[string]interface{}{"message": "invalid url path for maven"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	version := mvnElts[len(mvnElts)-2]
	//packageInput := mvnElts[len(mvnElts)-3]
	packageInput := strings.Join(mvnElts[0:len(mvnElts)-2], ".")

	f := gozilla.NewMavenFileObject(subjectInput, repoInput, packageInput, version, filePath)

	if strings.HasSuffix(version, "-SNAPSHOT") {
		ext := filepath.Ext(f.GetName())
		/*
			if strings.HasSuffix(ext, ".md5") || strings.HasSuffix(ext, ".sha1") {
				f.DoSkipIndex(true)
			}
		*/
		stripped := strings.Replace(filepath.Base(f.GetName()), "."+ext, "", 1)
		elts := strings.Split(stripped, "-")
		snapv := elts[1]
		ts := elts[2]
		//v := elts[1] + "-" + elts[2] + "-" + elts[3]
		buildElts := strings.Split(elts[3], ".")
		build := buildElts[0]
		v := fmt.Sprintf("%s-%s-%s", snapv, ts, build)
		f.SetExtra("ext", ext[1:])
		f.SetExtra("ts", ts)
		f.SetExtra("build", build)
		f.SetExtra("version", v)
		f.SetExtra("snapshot", true)
	} else {
		f.SetExtra("snapshot", false)
	}

	putContent(w, r, goz, claims, f, true)
}

//MavenMetadataGetHandler TODO
var MavenMetadataGetHandler = func(w http.ResponseWriter, r *http.Request) {
	// TODO fix timestamp format for  last updated info (following maven format)
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, _ := gozilla.CheckToken(goz, r.Header)

	goz.User = claims
	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	//packageInput := vars["package"]
	filePath := vars["path"]
	mvnElts := strings.Split(filePath, "/")
	if len(mvnElts) < 3 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		respError := map[string]interface{}{"message": "invalid url path for maven"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packageInput := mvnElts[len(mvnElts)-2]
	version := mvnElts[len(mvnElts)-1]

	globalMeta := false
	if version == packageInput {
		globalMeta = true
	}

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}
	_, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", subjectInput, repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if globalMeta {

		packSearch := gozilla.Package{
			Subject: subjectInput,
			Repo:    repoInput,
			ID:      packageInput,
		}
		versions, _ := packSearch.GetVersions(goz)
		var lastVersion gozilla.PackageVersion
		for _, v := range versions {
			if v.LastUpdated > lastVersion.LastUpdated {
				lastVersion = v
			}
		}

		type MetaVersions struct {
			GroupID     string
			ArtifactID  string
			LastVersion gozilla.PackageVersion
			Versions    []gozilla.PackageVersion
			NbVersions  int
		}

		metaVersions := MetaVersions{
			GroupID:     strings.Join(mvnElts[0:len(mvnElts)-1], "."),
			ArtifactID:  packageInput,
			LastVersion: lastVersion,
			Versions:    versions,
			NbVersions:  len(versions),
		}

		metadata := `<metadata modelVersion="1.1.0">
<groupId>{{.GroupID}}</groupId>
<artifactId>{{.ArtifactID}}</artifactId>
<versioning>
	{{ if gt .NbVersions  0 }}
	<release>{{.LastVersion.Version}}</release>
	<lastUpdated>{{.LastVersion.LastUpdated}}</lastUpdated>
	<versions>
	{{ range $v := .Versions}}
	<version>{{$v.Version}}</version>
	{{ end }}
	</versions>
	{{end}}
</versioning>
</metadata>
`
		tmpl, _ := template.New("metadata").Parse(metadata)
		var metaXML bytes.Buffer
		tmpl.Execute(&metaXML, metaVersions)

		w.Header().Add("Content-Type", "text/xml")
		w.Write([]byte(metaXML.String()))
		return
	}

	type MavenSnapshotVersion struct {
		Extension string
		TS        string
		Version   string
		Build     string
	}

	type MavenSnapshots struct {
		GroupID     string
		ArtifactID  string
		Version     string
		Versions    []MavenSnapshotVersion
		NbVersions  int
		LastVersion MavenSnapshotVersion
	}

	mvnSearch := gozilla.PackageVersion{
		Subject: subjectInput,
		Repo:    repoInput,
		Package: packageInput,
		Version: version,
	}
	mvnFiles, _ := mvnSearch.Files(goz)
	mvnSnapshots := MavenSnapshots{
		GroupID:    strings.Join(mvnElts[0:len(mvnElts)-2], "."),
		ArtifactID: packageInput,
		Version:    version,
		Versions:   make([]MavenSnapshotVersion, 0),
		NbVersions: 0,
	}
	var lastSnap MavenSnapshotVersion
	for _, snap := range mvnFiles {

		extras := snap.GetExtras()
		if _, ok := extras["ext"]; !ok {
			continue
		}
		ext := extras["ext"].(string)
		if strings.HasSuffix(ext, "sha1") || strings.HasSuffix(ext, "md5") {
			continue
		}
		ts := extras["ts"].(string)
		build := extras["build"].(string)
		v := extras["version"].(string)

		//ext := filepath.Ext(snap.GetName())
		//stripped := strings.Replace(filepath.Base(snap.GetName()), "."+ext, "", 1)
		//elts := strings.Split(stripped, "-")

		//v := elts[1] + "-" + elts[2] + "-" + elts[3]
		mvnSnap := MavenSnapshotVersion{
			Extension: ext,
			TS:        ts,
			Version:   v,
			Build:     build,
		}
		if mvnSnap.TS > lastSnap.TS {
			lastSnap = mvnSnap
		}
		mvnSnapshots.Versions = append(mvnSnapshots.Versions, mvnSnap)
	}
	mvnSnapshots.NbVersions = len(mvnSnapshots.Versions)
	mvnSnapshots.LastVersion = lastSnap

	metadata := `<metadata modelVersion="1.1.0">
	<groupId>{{.GroupID}}</groupId>
	<artifactId>{{.ArtifactID}}</artifactId>
	<version>{{.Version}}</version>
	<versioning>
	  {{ if gt .NbVersions  0 }}
	  <snapshot>
		<timestamp>{{.LastVersion.TS}}</timestamp>
		<buildNumber>{{.LastVersion.Build}}</buildNumber>
	  </snapshot>
	  <lastUpdated>{{.LastVersion.TS}}</lastUpdated>
	  <snapshotVersions>
	  {{ range $v := .Versions}}
		<snapshotVersion>
		  <extension>{{$v.Extension}}</extension>
		  <value>{{$v.Version}}</value>
		  <updated>{{$v.TS}}</updated>
		</snapshotVersion>
	  {{ end }}
	  </snapshotVersions>
	  {{end}}
	</versioning>
  </metadata>
`
	tmpl, _ := template.New("metadata").Parse(metadata)
	var metaXML bytes.Buffer
	tmpl.Execute(&metaXML, mvnSnapshots)

	w.Header().Add("Content-Type", "text/xml")
	w.Write([]byte(metaXML.String()))
	return
}

// MavenMetaPutHandler manage maven file download
var MavenMetaPutHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	resp := map[string]interface{}{"message": "uploaded"}
	json.NewEncoder(w).Encode(resp)
	return
}

// MavenGetHandler manage maven file download
var MavenGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	filePath := vars["path"]
	mvnElts := strings.Split(filePath, "/")
	if len(mvnElts) < 4 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		respError := map[string]interface{}{"message": "invalid url path for maven"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	packageInput := strings.Join(mvnElts[0:len(mvnElts)-2], ".")
	// packageInput := mvnElts[len(mvnElts)-3]
	version := mvnElts[len(mvnElts)-2]

	f := gozilla.NewMavenFileObject(subjectInput, repoInput, packageInput, version, filePath)

	getContent(w, r, goz, claims, claimsErr, f, true)
}

// ContentPutHandler manage file upload
var ContentPutHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	filePath := vars["path"]

	f := gozilla.NewRawFileObject(subjectInput, repoInput, packageInput, version, filePath)

	putContent(w, r, goz, claims, f, false)

}

// ContentDeleteHandler deletes file
var ContentDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	filePath := vars["path"]

	f := gozilla.NewRawFileObject(subjectInput, repoInput, packageInput, version, filePath)

	deleteContent(w, r, goz, claims, f, false)

}

// ContentGetHandler manage file upload
var ContentGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packageInput := vars["package"]
	version := vars["version"]
	filePath := vars["path"]

	f := gozilla.NewRawFileObject(subjectInput, repoInput, packageInput, version, filePath)

	getContent(w, r, goz, claims, claimsErr, f, false)

}

// SignedURL generates a signed URL to access a file by anyone
var SignedURL = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", claimsErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]
	packID := vars["package"]
	packVersion := vars["version"]
	filePath := vars["path"]

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", subjectInput, repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	_, isMember := repo.MemberOf(goz, claims)
	if !isMember {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": fmt.Sprintf("not member of %s", repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	f := gozilla.NewRawFileObject(subjectInput, repoInput, packID, packVersion, filePath)

	var expiresIn int64
	expiryMinutes, ok := r.URL.Query()["expiry"]
	if !ok || len(expiryMinutes) != 1 {
		expiresIn = 60 * 24 // 24h
	} else {
		var expiresErr error
		expiresIn, expiresErr = strconv.ParseInt(expiryMinutes[0], 10, 64)
		if expiresErr != nil {
			expiresIn = 60 * 24 // 24h
		}
	}

	type Signing struct {
		Expiry int64 `json:"expiry"`
	}

	signInput := &Signing{}
	err := json.NewDecoder(r.Body).Decode(signInput)
	if err == nil {
		expiresIn = signInput.Expiry
	}
	if expiresIn == 0 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cannot set an expiration to 0"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fh, _ := goz.Storage.GetObjectURL(goz, f, expiresIn)
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	respError := map[string]interface{}{"url": fh}
	json.NewEncoder(w).Encode(respError)
}

// GPGGetHandler returns server GPG public key
var GPGGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	if goz.Config.Gpg.Pubkeyfile == "" {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "no pub key available"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	file, err := os.Open(goz.Config.Gpg.Pubkeyfile)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "could not get pub key"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	defer file.Close()
	fileContents, err := ioutil.ReadAll(file)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "could not get pub key"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	w.Header().Add("Content-Type", "text/plain")
	w.WriteHeader(http.StatusOK)
	w.Write(fileContents)

}

// SearchHandler searches for files, packages within a repo
var SearchHandler = func(w http.ResponseWriter, r *http.Request) {
	pathPattern := ""
	full := false
	paths, ok := r.URL.Query()["path"]
	if ok && len(paths) > 0 {
		pathPattern = paths[0]
	}
	fulls, ok := r.URL.Query()["full"]
	if ok && len(fulls) > 0 {
		if fulls[0] == "1" || fulls[0] == "true" {
			full = true
		}
	}

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr == nil {
		goz.User = claims
	}

	vars := mux.Vars(r)
	subjectInput := vars["subject"]
	repoInput := vars["repo"]

	repoSearch := gozilla.Repo{
		Subject: subjectInput,
		ID:      repoInput,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("repository not found: %s/%s", subjectInput, repoInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if !repo.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed to access this repository"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	files, filesErr := repo.Files(goz)
	if filesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": filesErr}
		json.NewEncoder(w).Encode(respError)
		return
	}

	type SearchResult struct {
		Files map[string]map[string][]gozilla.FileObject
	}

	searchResult := SearchResult{
		Files: make(map[string]map[string][]gozilla.FileObject),
	}

	for _, file := range files {
		if pathPattern != "" {
			if !strings.Contains(file.GetPath(), pathPattern) {
				continue
			}
		}
		if _, ok := searchResult.Files[file.GetPackage()]; !ok {
			searchResult.Files[file.GetPackage()] = make(map[string][]gozilla.FileObject)
		}
		if _, ok := searchResult.Files[file.GetVersion()]; !ok {
			searchResult.Files[file.GetPackage()][file.GetVersion()] = make([]gozilla.FileObject, 0)
		}
		if full {
			vfiles := searchResult.Files[file.GetPackage()][file.GetVersion()]
			vfiles = append(vfiles, file)
			searchResult.Files[file.GetPackage()][file.GetVersion()] = vfiles
		}
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"hits": searchResult}
	json.NewEncoder(w).Encode(resp)
	return
}

// ContentAuthHandler checks token to authorize for GET/POST/PUT/DELETE
// Expects X-Original-URI header containing original request
// and X-Original-METHOD header containing original method
var ContentAuthHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	origRequestURI := r.Header.Get("X-Original-URI")
	origRequestMethod := r.Header.Get("X-Original-METHOD")
	if origRequestMethod == "" {
		origRequestMethod = "GET"
	}
	tokens, ok := r.URL.Query()["token"]
	if !ok || len(tokens) != 1 {
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("WWW-Authenticate", fmt.Sprintf("Bearer realm=\"%s/api/%s/user/auth\"", goz.Config.URL, currentAPIversion))
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": "no token provided"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	token := tokens[0]
	msg, msgErr := gozilla.FernetDecode(goz.Config, []byte(token), false)
	if msgErr != nil {
		log.Error().Str("uri", origRequestURI).Msg("invalid token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "invalid token"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	var fileToken gozilla.FileToken
	fileErr := json.Unmarshal(msg, &fileToken)
	if fileErr != nil {
		log.Error().Str("uri", origRequestURI).Msg("could not decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "invalid token content"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	access := true
	if origRequestMethod == "GET" || origRequestMethod == "HEAD" {
		if fileToken.Access == gozilla.FileACLNone {
			access = false
		}
	} else if fileToken.Access != gozilla.FileACLWrite {
		access = false
	}
	vars := mux.Vars(r)
	filePath := vars["path"]
	if strings.HasPrefix(origRequestURI, goz.Config.Storage.Prefix) {
		origRequestURI = strings.Replace(origRequestURI, goz.Config.Storage.Prefix, "", 1)
	}
	if origRequestURI[0] == '/' {
		origRequestURI = origRequestURI[1:]
	}
	log.Debug().Str("access", strconv.FormatBool(access)).Str("uri", origRequestURI).Str("tokenpath", fileToken.Path).Msg("Check path")

	if !strings.HasPrefix(origRequestURI, fileToken.Path) {
		access = false
	}
	if !access {
		log.Error().Str("uri", origRequestURI).Msg("access denied")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "operation not allowed", "path": filePath}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	respError := map[string]interface{}{"auth": access}
	json.NewEncoder(w).Encode(respError)
	return
}

// DockerLoginHandler TODO
var DockerLoginHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	if auth, ok := r.Header["Authorization"]; ok {
		authInfo := strings.Split(auth[0], " ")
		creds := authInfo[1]
		b64Creds, b64Err := b64.StdEncoding.DecodeString(creds)
		if b64Err != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": "invalid credentials"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		userCreds := strings.Split(string(b64Creds), ":")
		if len(userCreds) != 2 {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": "invalid credentials"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		// userid, apikey
		user, userErr := gozilla.UserAuth(goz, userCreds[0], userCreds[1])
		if userErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusUnauthorized)
			respError := map[string]interface{}{"message": "wrong credentials"}
			json.NewEncoder(w).Encode(respError)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		msg, _ := json.Marshal(gozilla.TokenMsg{User: user, Expire: 3600})
		token, _ := gozilla.FernetEncode(goz.Config, msg)
		resp := map[string]interface{}{"token": string(token)}
		json.NewEncoder(w).Encode(resp)

	} else {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": "no credentials provided"}
		json.NewEncoder(w).Encode(respError)
	}
}

// ProductCreateHandler creates a new product
var ProductCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductGetHandler shows product info
var ProductGetHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductListHandler list products
var ProductListHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, claimsErr := gozilla.CheckToken(goz, r.Header)
	if claimsErr == nil {
		goz.User = claims
	}

	vars := mux.Vars(r)
	subjectInput := vars["subject"]

	subjectSearch := gozilla.Subject{
		ID: subjectInput,
	}
	subject, subjectErr := subjectSearch.Exists(goz)
	if subjectErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": fmt.Sprintf("subject not found: %s", subjectInput)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if !subject.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "not allowed to access this repository"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	products, productsErr := subject.Products(goz)
	if productsErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"error": productsErr}
		json.NewEncoder(w).Encode(respError)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"subject": subject, "products": products}
	json.NewEncoder(w).Encode(resp)
}

// ProductDeleteHandler deletes a product (if no product version exists)
var ProductDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductVersionCreateHandler creates a new product version
var ProductVersionCreateHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductVersionGetHandler shows a product version info
var ProductVersionGetHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductVersionListHandler lists product versions
var ProductVersionListHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// ProductVersionDeleteHandler deletes a product version
var ProductVersionDeleteHandler = func(w http.ResponseWriter, r *http.Request) {
	log.Error().Msg("not yet implemented")
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusInternalServerError)
	respError := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(respError)
	return
}

// CasValidateHandler checks for CAS answer
var CasValidateHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	keys, ok := r.URL.Query()["ticket"]
	if !ok || len(keys[0]) < 1 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas validation failed, no ticket"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	ticket := keys[0]
	keys, ok = r.URL.Query()["server"]
	if !ok || len(keys[0]) < 1 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas validation failed, no server defined"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	casID := keys[0]
	var cas gozilla.CASServer
	for _, c := range goz.Config.Auth.CAS {
		if c.ID == casID {
			cas = c
		}
	}

	resp, err := http.Get(fmt.Sprintf("%s?service=%s/callback/cas/%s&ticket=%s", cas.ValidateURL, goz.Config.URL, casID, ticket))
	if err != nil {
		log.Error().Err(err).Msg("cas validation failed")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas validation failed"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("cas validation failed")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas validation failed"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	log.Debug().Msgf("CAS result %s", string(body))

	type CasAttributes struct {
		CN   string `xml:"cn"`
		Mail string `xml:"mail"`
	}

	type CasAuthSuccess struct {
		User       string        `xml:"user"`
		Attributes CasAttributes `xml:"attributes"`
	}

	type CasXML struct {
		ServiceResponse xml.Name       `xml:"serviceResponse"`
		AuthSuccess     CasAuthSuccess `xml:"authenticationSuccess"`
		AuthFailure     string         `xml:"authenticationFailure"`
	}

	var casResponse CasXML
	err = xml.Unmarshal(body, &casResponse)
	if err != nil {
		log.Error().Err(err).Msg("cas message error")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas message error"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	if casResponse.AuthFailure != "" {
		log.Error().Err(err).Msg("cas auth failure")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "cas auth failure"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	user := gozilla.User{
		ID:    casResponse.AuthSuccess.User,
		Email: casResponse.AuthSuccess.Attributes.Mail,
	}

	tokenmsg := gozilla.TokenMsg{
		User: user,
	}
	msg, _ := json.Marshal(tokenmsg)
	token, _ := gozilla.FernetEncode(goz.Config, msg)

	_, userErr := user.Exists(goz)
	if userErr != nil {
		log.Info().Str("user", user.ID).Str("cas", casID).Msgf("Create CAS user")
		user.APIKey = gozilla.Random(8)
		user.Origin = fmt.Sprintf("cas:%s", casID)
		if cas.Customer != "" {
			user.Customer = cas.Customer
		}
		user.Create(goz)
	}

	/*
		userSubject := gozilla.Subject{
			ID:         user.ID,
			Owner:      user.ID,
			Type:       gozilla.SubjectPersonal,
			Visibility: gozilla.VisibilityPublic,
		}

		// If user subject does not exists yet, create it
		_, subjectErr := userSubject.Exists(goz)
		if subjectErr != nil {
			subjectErr = userSubject.Create(goz)
			if subjectErr != nil {
				log.Error().Str("subject", user.ID).Msgf("failed to create subject")
			}
		}
	*/

	w.Header().Add("Content-Type", "application/json")
	respCas := map[string]interface{}{"user": user.ID, "token": string(token)}
	json.NewEncoder(w).Encode(respCas)

}

// ConanGetHandler fetch conan files
var ConanGetHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotImplemented)
	resp := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(resp)
}

// ConanPutHandler records conan files
var ConanPutHandler = func(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotImplemented)
	resp := map[string]interface{}{"message": "not yet implemented"}
	json.NewEncoder(w).Encode(resp)
}

// ConanPingHandler returns conan capabilities
var ConanPingHandler = func(w http.ResponseWriter, r *http.Request) {
	//w.Header().Add("X-Conan-Server-Capabilities", "complex_search,revisions")
	w.Header().Add("X-Conan-Server-Capabilities", "complex_search")
}

// ConanDigestHandler returns latest files
var ConanDigestHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Version: version,
		Package: pack,
		Repo:    repo,
		Subject: subject,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvErr := pv.Files(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "no file found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	lastTS := 0
	lastRREV := "0"
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; ok {
			continue
		}
		rrev := "0"
		if extraRREV, ok := extras["rrev"]; ok {
			rrev = extraRREV.(string)
		}
		if extraTS, ok := extras["ts"]; ok {
			pvts, err := strconv.Atoi(extraTS.(string))
			if err != nil {
				log.Error().Err(err).Str("file", pvFile.GetName()).Msg("invalid ts for file")
				continue
			}
			if pvts > lastTS {
				lastTS = pvts
				lastRREV = rrev
			}
		}
	}

	fileRefs := make(map[string]string)
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; ok {
			continue
		}
		if extraRREV, ok := extras["rrev"]; ok {
			if extraRREV.(string) != lastRREV {
				continue
			}
		}

		fileRef := gozilla.NewConanFileObject(subject, repo, pack, version, pvFile.GetName())
		fileRef.SetExtras(pvFile.GetExtras())

		fh, fErr := goz.Storage.GetObjectURL(goz, fileRef, 60)
		if fErr == nil {
			fileRefs[pvFile.GetName()] = fh
		}

	}

	if len(fileRefs) == 0 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "no file found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileRefs)

}

/*
var ConanPackPutHandler = func(w http.ResponseWriter, r *http.Request) {
	keys, ok := r.URL.Query()["signature"]
	if !ok || len(keys) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		w.Header().Add("Content-Type", "application/json")
		return
	}

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	msg, msgErr := gozilla.FernetDecode(goz.Config, []byte(keys[0]), false)
	if msgErr != nil {
		log.Error().Err(msgErr).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", msgErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	type UploadMsg struct {
		File gozilla.ConanFileObject
		User gozilla.User
	}
	var uploadMsg UploadMsg
	err := json.Unmarshal(msg, &uploadMsg)
	if err != nil {
		log.Error().Err(msgErr).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	path := vars["path"]
	prev := vars["prev"]
	rrev := vars["rrev"]
	packID := vars["packid"]

	fc := uploadMsg.File
	claims := uploadMsg.User

	if fc.Subject != subject || fc.Repo != repo || fc.Package != pack || fc.Version != version || fc.Path != path {
		log.Error().Err(msgErr).Msg("expecting to modify file different from token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": "expecting to modify file different from token"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	_, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Version: version,
		Package: pack,
		Repo:    repo,
		Subject: subject,
	}

	now := time.Now()
	ts := now.Unix()

	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		pvSearch.Create(goz)
	} else {
		pv.Save(goz)
	}

	f := gozilla.NewConanFileObject(subject, repo, pack, version, path)
	conanInfo := make(map[string]interface{})
	conanInfo["rrev"] = rrev
	conanInfo["prev"] = prev
	conanInfo["packid"] = packID
	conanInfo["ts"] = fmt.Sprintf("%d", ts)
	f.SetExtras(conanInfo)
	putContent(w, r, goz, claims, f, false)
}

var ConanPackGetHandler = func(w http.ResponseWriter, r *http.Request) {
	NYIHandler(w, r)
}

var ConanExportGetHandler = func(w http.ResponseWriter, r *http.Request) {
	NYIHandler(w, r)
}

var ConanExportPutHandler = func(w http.ResponseWriter, r *http.Request) {
	// PUT /v1/files/hello/0.1/subject/repo/0/export/conanmanifest.txt?signature=TOKENVALUE
	keys, ok := r.URL.Query()["signature"]
	if !ok || len(keys) == 0 {
		w.WriteHeader(http.StatusUnauthorized)
		w.Header().Add("Content-Type", "application/json")
		return
	}

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	msg, msgErr := gozilla.FernetDecode(goz.Config, []byte(keys[0]), false)
	if msgErr != nil {
		log.Error().Err(msgErr).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", msgErr)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	type UploadMsg struct {
		File gozilla.ConanFileObject
		User gozilla.User
	}
	var uploadMsg UploadMsg
	err := json.Unmarshal(msg, &uploadMsg)
	if err != nil {
		log.Error().Err(msgErr).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	path := vars["path"]

	rrev := vars["rrev"]

	fc := uploadMsg.File
	claims := uploadMsg.User

	if fc.Subject != subject || fc.Repo != repo || fc.Package != pack || fc.Version != version || fc.Name != path {
		log.Error().Err(msgErr).Msg("expecting to modify file different from token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": "expecting to modify file different from token"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	_, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	f := gozilla.NewConanFileObject(subject, repo, pack, version, path)
	info := make(map[string]interface{})
	info["rrev"] = rrev
	f.SetExtras(info)

	// TODO Set revision to zero or increase in vpack and file?
	putContent(w, r, goz, claims, f, false)
}
*/

// ConanCheckAuthHandler checks for user token
var ConanCheckAuthHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	w.Header().Add("Content-Type", "text/plain")
	w.Write([]byte(claims.ID))
}

// ConanAuthHandler handles conan authentication
var ConanAuthHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	user := claims
	w.Header().Add("Content-Type", "text/plain")
	tokenmsg := gozilla.TokenMsg{
		User: user,
	}
	msg, _ := json.Marshal(tokenmsg)
	token, _ := gozilla.FernetEncode(goz.Config, msg)

	/*
		userSubject := gozilla.Subject{
			ID:         user.ID,
			Owner:      user.ID,
			Type:       gozilla.SubjectPersonal,
			Visibility: gozilla.VisibilityPublic,
		}
		// If user subject does not exists yet, create it
		_, subjectErr := userSubject.Exists(goz)
		if subjectErr != nil {
			subjectErr = userSubject.Create(goz)
			if subjectErr != nil {
				log.Error().Str("subject", user.ID).Msgf("failed to create subject")
			}
		}
	*/
	w.Write(token)
}

/*
var ConanPackageGetHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Version: version,
		Package: pack,
		Repo:    repo,
		Subject: subject,
	}
	_, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"message": "access allowed"}
	json.NewEncoder(w).Encode(resp)
}
*/

// ConanPackUploadURLSHandler returns URLS to upload input files
var ConanPackUploadURLSHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	packID := vars["packid"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanWrite(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var uploads map[string]int
	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&uploads)
	if err != nil {
		log.Error().Err(err).Msg("upload_urls failed")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "body content error"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	initDone := false

	fileUploads := make(map[string]string)
	for key := range uploads {
		fileToUpload := gozilla.NewConanFileObject(subject, repo, pack, version, key)
		info := make(map[string]interface{})
		info["packid"] = packID
		fileToUpload.SetExtras(info)

		if !initDone {
			err := checkPutContent(w, r, goz, claims, fileToUpload)
			if err != nil {
				log.Error().Err(err).Msg("an error occured")
				return
			}
			initDone = true
		}

		fh, fErr := saveFile(goz, r, w, fileToUpload)
		if fErr != nil {
			log.Error().Err(fErr).Msgf("failed to save file")
			return
		}
		// Add fake signature because conan expects it
		fileUploads[key] = fmt.Sprintf("%s&signature=conan", fh)
		//fileUploads[key] = fh
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileUploads)
}

// ConanPackHandler checks if package id exists
var ConanPackHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	packID := vars["packid"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Version: version,
		Package: pack,
		Repo:    repo,
		Subject: subject,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvFilesErr := pv.Files(goz)
	if pvFilesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package id not found"}
		json.NewEncoder(w).Encode(respError)
	}

	packFiles := make(map[string]string)
	packIDFound := false
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if pvPackID, ok := extras["packid"]; ok {
			if pvPackID.(string) == packID {
				packIDFound = true
				fi, fiErr := goz.Storage.MD5(goz, pvFile)
				sha := ""
				if fiErr == nil {
					sha = fi.MD5
				}
				packFiles[pvFile.GetName()] = sha
			}
		}
	}

	if packIDFound {
		w.Header().Add("Content-Type", "application/json")
		json.NewEncoder(w).Encode(packFiles)
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusNotFound)
	respError := map[string]interface{}{"message": "package id not found"}
	json.NewEncoder(w).Encode(respError)
}

// ConanPackDigestHandler returns latest files for conan package id
var ConanPackDigestHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	// Allow anonymous, additional controls will be done later on
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	packID := vars["packid"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Version: version,
		Package: pack,
		Repo:    repo,
		Subject: subject,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvErr := pv.Files(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "no file found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	lastTS := 0
	lastPREV := "0"
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; !ok {
			continue
		}
		if extras["packid"].(string) != packID {
			continue
		}
		prev := "0"
		if extraPREV, ok := extras["prev"]; ok {
			prev = extraPREV.(string)
		}
		if extraTS, ok := extras["ts"]; ok {
			pvts, err := strconv.Atoi(extraTS.(string))
			if err != nil {
				log.Error().Err(err).Str("file", pvFile.GetName()).Msg("invalid ts for file")
				continue
			}
			if pvts > lastTS {
				lastTS = pvts
				lastPREV = prev
			}
		}
	}

	fileRefs := make(map[string]string)
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; !ok {
			continue
		}
		if extras["packid"].(string) != packID {
			continue
		}
		if extraPREV, ok := extras["prev"]; ok {
			if extraPREV.(string) != lastPREV {
				continue
			}
		}

		fileRef := gozilla.NewConanFileObject(subject, repo, pack, version, pvFile.GetName())
		fileRef.SetExtras(pvFile.GetExtras())

		fh, fErr := goz.Storage.GetObjectURL(goz, fileRef, 60)
		if fErr == nil {
			fileRefs[pvFile.GetName()] = fh
		}

	}

	if len(fileRefs) == 0 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "no file found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileRefs)
}

var conanFileDownload = func(w http.ResponseWriter, r *http.Request, f gozilla.FileObject) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	repoSearch := gozilla.Repo{
		ID:      f.GetRepo(),
		Subject: f.GetSubject(),
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fh, fhErr := goz.Storage.GetObjectURL(goz, f, 3600)
	if fhErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(fhErr)
		return
	}

	w.Header().Add("Location", fh)
	w.WriteHeader(http.StatusTemporaryRedirect)
}

// ConanPackageFileGetHandler downlads requested file from package
var ConanPackageFileGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	rrev := vars["rrev"]
	prev := vars["prev"]
	packID := vars["packid"]
	path := vars["path"]

	f := gozilla.NewConanFileObject(subject, repo, pack, version, path)
	extras := make(map[string]interface{})
	extras["rrev"] = rrev
	extras["prev"] = prev
	extras["packid"] = packID
	f.SetExtras(extras)
	conanFileDownload(w, r, f)
}

// ConanExportFileGetHandler downloads requested file from export folder
var ConanExportFileGetHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	rrev := vars["rrev"]
	path := vars["path"]

	f := gozilla.NewConanFileObject(subject, repo, pack, version, path)
	extras := make(map[string]interface{})
	extras["rrev"] = rrev
	f.SetExtras(extras)
	conanFileDownload(w, r, f)
}

// ConanPackDownloadURLsHandler returns URLS to download
var ConanPackDownloadURLsHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]
	packID := vars["packid"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fileDownloads := make(map[string]string)
	pvSearch := gozilla.PackageVersion{
		Subject: subject,
		Repo:    repo,
		Package: pack,
		Version: version,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvFilesErr := pv.Files(goz)
	if pvFilesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; !ok {
			continue
		}
		if extras["packid"].(string) != packID {
			continue
		}
		fh, fhErr := goz.Storage.GetObjectURL(goz, pvFile, 60)
		if fhErr != nil {
			continue
		}
		fileDownloads[pvFile.GetName()] = fmt.Sprintf("%s&signature=conan", fh)
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileDownloads)
}

// ConanDownloadURLsHandler returns URLS to download
var ConanDownloadURLsHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	fileDownloads := make(map[string]string)
	pvSearch := gozilla.PackageVersion{
		Subject: subject,
		Repo:    repo,
		Package: pack,
		Version: version,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvFilesErr := pv.Files(goz)
	if pvFilesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	for _, pvFile := range pvFiles {
		extras := pvFile.GetExtras()
		if _, ok := extras["packid"]; ok {
			continue
		}
		fh, fhErr := goz.Storage.GetObjectURL(goz, pvFile, 60)
		if fhErr != nil {
			continue
		}
		fileDownloads[pvFile.GetName()] = fmt.Sprintf("%s&signature=conan", fh)
	}

	gozilla.StatDownload(goz, subject, repo, pack, gozilla.ConanPackage)

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileDownloads)

}

// ConanSearchHandler returns conaninfo content in json
var ConanSearchHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckTokenOrAnonymous(goz, r.Header, true)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanRead(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvSearch := gozilla.PackageVersion{
		Subject: subject,
		Repo:    repo,
		Package: pack,
		Version: version,
	}
	pv, pvErr := pvSearch.Exists(goz)
	if pvErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	pvFiles, pvFilesErr := pv.Files(goz)
	if pvFilesErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	conanInfos := make(map[string]interface{})
	foundSomething := false

	for _, pvFile := range pvFiles {
		if pvFile.GetName() == "conaninfo.txt" {
			extras := pvFile.GetExtras()
			if _, ok := extras["packid"]; ok {
				packid := extras["packid"].(string)
				conanInfo, conanInfoErr := gozilla.ConanInfo(goz, pvFile)
				if conanInfoErr != nil {
					log.Error().Err(conanInfoErr).Msg("failed to get conan info")
					continue
				}
				conanInfos[packid] = conanInfo
				foundSomething = true
			}
		}
	}
	if !foundSomething {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "package version not found"}
		json.NewEncoder(w).Encode(respError)
	}

	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(conanInfos)

}

// ConanUploadURLsHandler returns URLS to upload input files
var ConanUploadURLsHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	claims, err := gozilla.CheckToken(goz, r.Header)
	if err != nil {
		log.Error().Err(err).Msg("failed to decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		respError := map[string]interface{}{"message": fmt.Sprintf("Auth error: %s", err)}
		json.NewEncoder(w).Encode(respError)
		return
	}
	goz.User = claims

	vars := mux.Vars(r)

	subject := vars["subject"]
	repo := vars["repo"]
	pack := vars["package"]
	version := vars["version"]

	repoSearch := gozilla.Repo{
		ID:      repo,
		Subject: subject,
	}
	repoDB, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusNotFound)
		respError := map[string]interface{}{"message": "repo not found"}
		json.NewEncoder(w).Encode(respError)
		return
	}
	if !repoDB.CanWrite(goz) {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		respError := map[string]interface{}{"message": "access denied"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	var uploads map[string]int
	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&uploads)
	if err != nil {
		log.Error().Err(err).Msg("upload_urls failed")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		respError := map[string]interface{}{"message": "body content error"}
		json.NewEncoder(w).Encode(respError)
		return
	}

	initDone := false

	gozilla.StatUpload(goz, subject, repo, pack, gozilla.ConanPackage)
	pvu := gozilla.PackageVersionUpdate{
		Subject: subject,
		Repo:    repo,
		Package: pack,
		Version: version,
	}
	pvu.New(goz)

	fileUploads := make(map[string]string)
	for key := range uploads {
		fileToUpload := gozilla.NewConanFileObject(subject, repo, pack, version, key)

		if !initDone {
			err := checkPutContent(w, r, goz, claims, fileToUpload)
			if err != nil {
				log.Error().Err(err).Msg("an error occured")
				return
			}
			initDone = true
		}

		fh, fErr := saveFile(goz, r, w, fileToUpload)
		if fErr != nil {
			log.Error().Err(fErr).Msgf("failed to save file")
			return
		}

		// Add fake signature because conan expects it
		fileUploads[key] = fmt.Sprintf("%s&signature=conan", fh)
	}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(fileUploads)
}

// GeneralSearchHandler TODO
var GeneralSearchHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)
	keys, ok := r.URL.Query()["q"]
	if !ok || len(keys) == 0 {
		w.WriteHeader(http.StatusNotFound)
		w.Header().Add("Content-Type", "application/json")
		return
	}
	search := keys[0]
	elts, eltsErr := gozilla.GozElement{}.Search(goz, search)
	if eltsErr != nil {
		log.Error().Err(eltsErr).Str("search", search).Msg("search error")
		w.WriteHeader(http.StatusNotFound)
		w.Header().Add("Content-Type", "application/json")
		return
	}
	w.Header().Add("Content-Type", "application/json")
	resp := map[string]interface{}{"search": elts}
	json.NewEncoder(w).Encode(resp)

}

// DockerAuthHandler TODO
var DockerAuthHandler = func(w http.ResponseWriter, r *http.Request) {
	goz := r.Context().Value(favContextKey("goz")).(gozilla.GozContext)

	type DockerError struct {
		Code    string `json:"code"`
		Message string `json:"message"`
	}
	type DockerErrors struct {
		Errors []DockerError `json:"errors"`
	}

	origRequestURI := r.Header.Get("X-Original-URI")
	origRequestMethod := r.Header.Get("X-Original-METHOD")
	origRequestHost := r.Header.Get("X-Original-Host")

	log.Error().Msgf("Request %s:%s", origRequestMethod, origRequestURI)

	if origRequestMethod == "" {
		origRequestMethod = "GET"
	}

	authBearer := r.Header.Get("Authorization")
	if authBearer == "" {
		log.Debug().Str("uri", origRequestURI).Msg("no token, request auth")
		w.Header().Add("Content-Type", "application/json")
		w.Header().Add("Www-Authenticate", fmt.Sprintf("Bearer realm=\"%s/api/%s/dockerlogin\"", origRequestHost, currentAPIversion))
		w.WriteHeader(http.StatusUnauthorized)
		//respError := map[string]interface{}{"message": "no token provided"}
		//json.NewEncoder(w).Encode(respError)
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: "no token provided"}}})
		return
	}

	requestedURI := strings.Replace(origRequestURI, "/v2/", "", 1)
	access := false

	if requestedURI == "" {
		access = true
	}

	uriElts := strings.Split(requestedURI, "/")
	if len(uriElts) == 1 {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		//respError := map[string]interface{}{"auth": access}
		//json.NewEncoder(w).Encode(respError)
		return
	}

	requestedSubjectRepo := uriElts[0]
	requestedPackage := uriElts[1]
	requestedTag := "latest"
	if uriElts[len(uriElts)-2] == "manifests" {
		requestedTag = uriElts[len(uriElts)-1]
	}

	requestedPath := strings.Split(requestedSubjectRepo, "-")
	if len(requestedPath) != 2 {
		log.Error().Str("uri", origRequestURI).Msg("invalid subject/repo name")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		//respError := map[string]interface{}{"message": "docker repo syntax must be subject-repo/package:tag"}
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: "docker repo syntax must be subject-repo/package:tag"}}})
		return
	}
	requestedSubject := requestedPath[0]
	requestedRepo := requestedPath[1]

	repoSearch := gozilla.Repo{
		ID:      requestedRepo,
		Subject: requestedSubject,
	}
	repo, repoErr := repoSearch.Exists(goz)
	if repoErr != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		//respError := map[string]interface{}{"message": fmt.Sprintf("subject/repo not found: %s %s", requestedSubject, requestedRepo)}
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: fmt.Sprintf("subject/repo not found: %s %s", requestedSubject, requestedRepo)}}})
		//json.NewEncoder(w).Encode(respError)
		return
	}

	// read request on public repo, OK
	if (origRequestMethod == "GET" || origRequestMethod == "HEAD") && repo.Visibility == gozilla.VisibilityPublic {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		respError := map[string]interface{}{"auth": access}
		json.NewEncoder(w).Encode(respError)
		return
	}

	authInfo := strings.Split(authBearer, " ")
	token := authInfo[1]
	msg, msgErr := gozilla.FernetDecode(goz.Config, []byte(token), false)
	if msgErr != nil {
		log.Error().Str("uri", origRequestURI).Msg("invalid token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		//respError := map[string]interface{}{"message": "invalid token"}
		//json.NewEncoder(w).Encode(respError)
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: "invalid token"}}})
		return
	}

	var accessToken gozilla.TokenMsg
	fileErr := json.Unmarshal(msg, &accessToken)
	if fileErr != nil {
		log.Error().Str("uri", origRequestURI).Msg("could not decode token")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		//respError := map[string]interface{}{"message": "invalid token content"}
		//json.NewEncoder(w).Encode(respError)
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: "invalid token content"}}})

		return
	}

	profile, isMember := repo.MemberOf(goz, accessToken.User)
	if !isMember {
		if (origRequestMethod == "GET" || origRequestMethod == "HEAD") && repo.Visibility < gozilla.VisibilityPrivate {
			access = true
		} else {
			access = false
		}
	} else {
		if profile >= gozilla.ProfileAdmin && profile < gozilla.ProfileGuest {
			access = true
		} else {
			if origRequestMethod == "GET" || origRequestMethod == "HEAD" {
				access = true
			}
		}
	}

	if !access {
		log.Error().Str("uri", origRequestURI).Msg("access denied")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusForbidden)
		//respError := map[string]interface{}{"message": "operation not allowed"}
		//json.NewEncoder(w).Encode(respError)
		json.NewEncoder(w).Encode(DockerErrors{Errors: []DockerError{DockerError{Code: "UNAUTHORIZED", Message: "operation not permitted"}}})

		return
	}

	if origRequestMethod == "PATCH" || origRequestMethod == "POST" || origRequestMethod == "PUT" {
		if uriElts[len(uriElts)-2] == "manifests" {
			packSearch := gozilla.Package{
				ID:      fmt.Sprintf("%s-docker", requestedPackage),
				Subject: requestedSubject,
				Repo:    requestedRepo,
			}
			_, packErr := packSearch.Exists(goz)
			if packErr != nil {
				log.Debug().Str("package", requestedPackage).Str("subject", requestedSubject).Str("repo", requestedRepo).Msg("new docker package")
				packSearch.Type = gozilla.DockerPackage
				packSearch.Create(goz)
			}

			vpackSearch := gozilla.PackageVersion{
				Package: fmt.Sprintf("%s-docker", requestedPackage),
				Subject: requestedSubject,
				Repo:    requestedRepo,
				Version: requestedTag,
			}
			_, vpackErr := vpackSearch.Exists(goz)
			if vpackErr != nil {
				log.Debug().Str("package", requestedPackage).Str("subject", requestedSubject).Str("repo", requestedRepo).Str("version", requestedTag).Msg("new docker package version")
				vpackSearch.Create(goz)
			}
		}
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	respError := map[string]interface{}{"auth": access}
	json.NewEncoder(w).Encode(respError)
	return
}

func setRoutes(r *mux.Router) {
	r.HandleFunc("/api", HomeHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/subjects/{subject}", SubjectGetHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/repos", SubjectCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/repos", SubjectListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/repos/{subject}", SubjectUpdateHandler).Methods("PATCH")
	r.HandleFunc("/api/{apiversion}/repos/{subject}", SubjectDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/repos/{subject}", RepoListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/repos/{subject}", RepoCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/repos/{subject}/{repo}", RepoGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/repos/{subject}/{repo}", RepoDeleteHandler).Methods("DELETE")
	r.HandleFunc("/api/{apiversion}/repos/{subject}/{repo}", RepoUpdateHandler).Methods("PATCH")

	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}", PackageListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}", PackageGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}", PackageDeleteHandler).Methods("DELETE")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}", PackageUpdateHandler).Methods("PATCH")

	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}", PackageCreateHandler).Methods("POST")

	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}/versions/{version}", PackageVersionGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}/versions/{version}", PackageVersionDeleteHandler).Methods("DELETE")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}/versions/{version}", PackageVersionUpdateHandler).Methods("PATCH")

	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}/versions", PackageVersionCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/packages/{subject}/{repo}/{package}/versions", PackageVersionListHandler).Methods("GET")

	// products
	r.HandleFunc("/api/{apiversion}/products/{subject}", ProductCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/products/{subject}", ProductListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}", ProductGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}", ProductDeleteHandler).Methods("DELETE")
	// product version
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}/versions", ProductVersionCreateHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}/versions", ProductVersionListHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}/versions/{version}", ProductVersionGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/products/{subject}/{product}/versions/{version}", ProductVersionDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/content/{subject}/{repo}/{package}/{version}/publish", PublishHandler).Methods("POST")

	r.HandleFunc("/api/{apiversion}/content/{subject}/{repo}/{package}/{version}/{path:.*}", ContentPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/content/{subject}/{repo}/{package}/{version}/{path:.*}", ContentGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/api/{apiversion}/content/{subject}/{repo}/{package}/{version}/{path:.*}", ContentDeleteHandler).Methods("DELETE")

	r.HandleFunc("/api/{apiversion}/auth", ContentAuthHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/dockerauth", DockerAuthHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/dockerlogin", DockerLoginHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/signed_url/{subject}/{repo}/{package}/{version}/{path:.*}", SignedURL).Methods("POST")

	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}/maven-metadata.xml", MavenMetaPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}/maven-metadata.xml.md5", MavenMetaPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}/maven-metadata.xml.sha1", MavenMetaPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}", MavenPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}/maven-metadata.xml", MavenMetadataGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/maven/{subject}/{repo}/{package}/{path:.*}", MavenGetHandler).Methods("GET")

	// Conan
	// r.HandleFunc("/api/{apiversion}/conan/{subject}/{repo}/{package}/{path:.*}", ConanPutHandler).Methods("PUT")
	// r.HandleFunc("/api/{apiversion}/conan/{subject}/{repo}/{package}/{path:.*}", ConanGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/ping", ConanPingHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/digest", ConanDigestHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/download_urls", ConanDownloadURLsHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/upload_urls", ConanUploadURLsHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/search", ConanSearchHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/packages/{packid}", ConanPackHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/packages/{packid}/digest", ConanPackDigestHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/packages/{packid}/upload_urls", ConanPackUploadURLSHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/conan/v1/conans/{package}/{version}/{subject}/{repo}/packages/{packid}/download_urls", ConanPackDownloadURLsHandler).Methods("GET")

	//r.HandleFunc("/api/{apiversion}/conan/v1/files/{package}/{version}/{subject}/{repo}/{rrev}/export/{path:.*}", ConanExportGetHandler).Methods("GET")
	//r.HandleFunc("/api/{apiversion}/conan/v1/files/{package}/{version}/{subject}/{repo}/{rrev}/export/{path:.*}", ConanExportPutHandler).Methods("PUT")
	//r.HandleFunc("/api/{apiversion}/conan/v1/files/{package}/{version}/{subject}/{repo}/{rrev}/package/{packid}/{prev}/{path:.*}", ConanPackGetHandler).Methods("GET")
	//r.HandleFunc("/api/{apiversion}/conan/v1/files/{package}/{version}/{subject}/{repo}/{rrev}/package/{packid}/{prev}/{path:.*}", ConanPackPutHandler).Methods("PUT")

	r.HandleFunc("/api/{apiversion}/conan/v1/users/check_credentials", ConanCheckAuthHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/conan/v1/users/authenticate", ConanAuthHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/search/{subject}/{repo}", SearchHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/debian/{subject}/{repo}/{package}/{version}/{path:.*}", DebianPutHandler).Methods("PUT")
	r.HandleFunc("/api/{apiversion}/debian/{subject}/{repo}/public/{path:.*}", DebianRepoGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/api/{apiversion}/debian/{subject}/{repo}/{package}/{version}/{path:.*}", DebianGetHandler).Methods("GET", "HEAD")

	// expected path distrib/arch/file.rpm
	r.HandleFunc("/api/{apiversion}/rpm/{subject}/{repo}/{package}/{version}/{path:.*}", RpmPutHandler).Methods("PUT")
	// Will contain repo arch/file.rpm and metadata dir
	r.HandleFunc("/api/{apiversion}/rpm/{subject}/{repo}/public/{path:.*}", RpmRepoGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/api/{apiversion}/rpm/{subject}/{repo}/{package}/{version}/{path:.*}", RpmGetHandler).Methods("GET", "HEAD")

	r.HandleFunc("/api/{apiversion}/gpg", GPGGetHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/search", GeneralSearchHandler).Methods("GET")

	r.HandleFunc("/api/{apiversion}/user", UserInfoHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/user/auth", AuthBindHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/user/auth/cas", CasValidateHandler).Methods("GET")
	r.HandleFunc("/api/{apiversion}/user/edit/password", UserPasswordHandler).Methods("POST")
	r.HandleFunc("/api/{apiversion}/user/edit/apikey", UserAPIKeyHandler).Methods("POST")

	r.HandleFunc("/dl/{subject}/{repo}/conan/v1/files/{package}/{version}/{rrev}/package/{packid}/{prev}/{path:.*}", ConanPackageFileGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/dl/{subject}/{repo}/conan/v1/files/{package}/{version}/{rrev}/export/{path:.*}", ConanExportFileGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/dl/{subject}/{repo}/maven/{path:.*}/maven-metadata.xml", MavenMetadataGetHandler).Methods("GET")
	r.HandleFunc("/dl/{subject}/{repo}/maven/{path:.*}", MavenGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/dl/{subject}/{repo}/debian/{path:.*}", DebianRepoGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/dl/{subject}/{repo}/rpm/{path:.*}", RpmRepoGetHandler).Methods("GET", "HEAD")
	r.HandleFunc("/dl/gpg", GPGGetHandler).Methods("GET")
	r.HandleFunc("/dl/{subject}/{repo}/raw/{package}/{version}/{path:.*}", ContentGetHandler).Methods("GET", "HEAD")
	r.Handle("/metrics", promhttp.Handler())
}

func gozMiddleware(gozCtx gozilla.GozContext, next http.Handler) http.Handler {
	goz := favContextKey("goz")
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := context.WithValue(r.Context(), goz, gozCtx)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	config := gozilla.LoadConfig()

	mongoClient, err := mongo.NewClient(mongoOptions.Client().ApplyURI(config.Mongo.URL))
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}
	ctx, cancelMongo := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancelMongo()

	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Error().Msgf("Failed to connect to mongo server %s\n", config.Mongo.URL)
		os.Exit(1)
	}

	storage, storageErr := gozilla.NewStorageHandler(config)
	if storageErr != nil {
		log.Error().Err(storageErr).Msg("storage error")
		os.Exit(1)
	}

	amqpHandler, amqpErr := gozilla.NewAmqpHandler(config)
	if amqpErr != nil {
		log.Error().Err(amqpErr).Msg("rabbitmq error")
		os.Exit(1)
	}

	esCfg := elasticsearch.Config{
		Addresses: config.Elastic,
	}
	es, err := elasticsearch.NewClient(esCfg)
	if err != nil {
		log.Error().Err(err).Msgf("Error creating the client: %s", err)
		os.Exit(1)
	}

	gozCtx := gozilla.GozContext{
		Mongo:       mongoClient,
		Config:      config,
		Storage:     storage,
		AmqpHandler: amqpHandler,
		Elastic:     es,
	}

	consulErr := gozilla.ConsulDeclare(gozCtx, "gozilla")
	if consulErr != nil {
		log.Error().Err(consulErr).Msgf("Failed to register: %s", consulErr.Error())
		os.Exit(1)
	}

	prometheus.MustRegister(RequestDuration)

	r := mux.NewRouter()
	setRoutes(r)
	promHandler := middleware.Instrument{
		RouteMatcher: r,
		Duration:     RequestDuration,
	}.Wrap(r)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowedMethods:   []string{"HEAD", "GET", "PATCH", "POST", "PUT", "DELETE"},
	})
	handler := c.Handler(promHandler)

	loggedRouter := handlers.LoggingHandler(os.Stdout, handler)
	ctxHandler := gozMiddleware(gozCtx, loggedRouter)

	srv := &http.Server{
		Handler: ctxHandler,
		Addr:    fmt.Sprintf("%s:%d", config.Web.Listen, config.Web.Port),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	srv.ListenAndServe()

}

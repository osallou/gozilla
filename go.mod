module gitlab.inria.fr/osallou/gozilla

go 1.13

require (
	github.com/armon/go-metrics v0.3.3 // indirect
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/gogo/googleapis v1.4.0 // indirect
	github.com/gogo/status v1.1.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gophercloud/gophercloud v0.11.0 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/hashicorp/consul/api v1.4.0 // indirect
	github.com/hashicorp/go-hclog v0.13.0 // indirect
	github.com/hashicorp/go-immutable-radix v1.2.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/hashicorp/serf v0.9.2 // indirect
	github.com/influxdata/influxdb-client-go v0.1.5
	github.com/influxdata/influxdb1-client v0.0.0-20200515024757-02f0bf5dbca3
	github.com/klauspost/compress v1.10.5 // indirect
	github.com/mitchellh/mapstructure v1.3.0 // indirect
	github.com/prometheus/client_golang v1.6.0
	github.com/prometheus/common v0.10.0 // indirect
	github.com/rs/cors v1.7.0
	github.com/rs/zerolog v1.18.0
	github.com/sirupsen/logrus v1.6.0 // indirect
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	github.com/tidwall/pretty v1.0.1 // indirect
	github.com/uber/jaeger-client-go v2.23.1+incompatible // indirect
	github.com/uber/jaeger-lib v2.2.0+incompatible // indirect
	github.com/weaveworks/common v0.0.0-20200512154658-384f10054ec5
	gitlab.inria.fr/osallou/gozilla-lib v0.0.0-20200520085924-45452f1744aa
	go.mongodb.org/mongo-driver v1.3.3
	go.uber.org/atomic v1.6.0 // indirect
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
	golang.org/x/lint v0.0.0-20200302205851-738671d3881b // indirect
	golang.org/x/net v0.0.0-20200520004742-59133d7f0dd7 // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
	golang.org/x/sys v0.0.0-20200519105757-fe76b779f299 // indirect
	golang.org/x/tools v0.0.0-20200319210407-521f4a0cd458 // indirect
	google.golang.org/genproto v0.0.0-20200519141106-08726f379972 // indirect
	google.golang.org/grpc v1.29.1 // indirect
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/yaml.v2 v2.3.0 // indirect
)

package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Version of server
var Version string

// HomeHandler manages base entrypoint
var HomeHandler = func(w http.ResponseWriter, r *http.Request) {
	resp := map[string]interface{}{"version": Version, "message": "ok"}
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resp)
}

// ContentDeleteHandler delete files
var ContentDeleteHandler = func(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	filePath := vars["path"]

	dir, _ := os.Getwd()
	dir = path.Join(dir, "data")
	if os.Getenv("GOZ_DIR") != "" {
		dir = os.Getenv("GOZ_DIR")
	}

	fullFilePath := path.Join(dir, filePath)

	os.Remove(fullFilePath)
	os.Remove(fullFilePath + ".md5")

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	resp := map[string]interface{}{"message": "deleted"}
	json.NewEncoder(w).Encode(resp)
	return
}

// ContentPutHandler manage file upload
var ContentPutHandler = func(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	filePath := vars["path"]

	dir, _ := os.Getwd()
	dir = path.Join(dir, "data")
	if os.Getenv("GOZ_DIR") != "" {
		dir = os.Getenv("GOZ_DIR")
	}

	maxUpload := os.Getenv("GOZ_MAXUPLOAD")
	var maxSize int64 = 5000000000000
	if maxUpload != "" {
		maxSize, _ = strconv.ParseInt(maxUpload, 10, 64)
	}

	var fileBytes []byte
	readBody := true
	if ct, ok := r.Header["Content-Type"]; ok {
		log.Debug().Msgf("content-type %s", ct[0])
		if strings.HasPrefix(ct[0], "multipart/form-data") {
			r.ParseMultipartForm(maxSize)
			log.Debug().Msg("read multipart/form-data")
			readBody = false
			file, _, err := r.FormFile("file")
			if err != nil {
				log.Error().Msg("invalid form data")
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(http.StatusBadRequest)
				resp := map[string]interface{}{"message": "*file* not found in multipart form"}
				json.NewEncoder(w).Encode(resp)
				return
			}
			defer file.Close()
			fileBytes, err = ioutil.ReadAll(file)
			if err != nil {
				log.Error().Msg("failed reading body")
			}
		}
	}
	if readBody {
		log.Debug().Msg("defaulting to body reading")
		defer r.Body.Close()
		var bodyErr error
		fileBytes, bodyErr = ioutil.ReadAll(r.Body)
		if bodyErr != nil {
			w.Header().Add("Content-Type", "application/json")
			w.WriteHeader(http.StatusBadRequest)
			resp := map[string]interface{}{"message": "error reading body"}
			json.NewEncoder(w).Encode(resp)
			return
		}
	}

	fullFilePath := path.Join(dir, filePath)
	//log.Debug().Msgf("Uploaded file %s to %s\n", handler.Filename, fullFilePath)
	//log.Debug().Msgf("File Size: %d\n", handler.Size)

	baseDir := path.Dir(fullFilePath)
	if _, err := os.Stat(baseDir); os.IsNotExist(err) {
		os.MkdirAll(baseDir, os.ModePerm)
	}

	tempFile, err := os.Create(fullFilePath)
	if err != nil {
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		resp := map[string]interface{}{"message": fmt.Sprintf("could not create file: %s", err)}
		json.NewEncoder(w).Encode(resp)
		return
	}
	defer tempFile.Close()

	tempFile.Write(fileBytes)

	hasher := md5.New()
	hasher.Write(fileBytes)
	md := md5.Sum(fileBytes)
	log.Debug().Str("file", fullFilePath).Msg("compute md5sum")
	mdFile, err := os.Create(fullFilePath + ".md5")
	if err == nil {
		mdFile.WriteString(hex.EncodeToString(md[:]))
	}
	defer mdFile.Close()

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	resp := map[string]interface{}{"message": "uploaded"}
	json.NewEncoder(w).Encode(resp)
	return
}

// ContentStatHandler returns some information about file
var ContentStatHandler = func(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	filePath := vars["path"]
	dir, _ := os.Getwd()
	dir = path.Join(dir, "data")
	if os.Getenv("GOZ_DIR") != "" {
		dir = os.Getenv("GOZ_DIR")
	}
	fullFilePath := path.Join(dir, filePath)
	md5Sum := ""
	var size int64
	var lastUpdated int64
	md5, err := ioutil.ReadFile(fullFilePath + ".md5")
	if err == nil {
		md5Sum = string(md5)
	}
	fs, fsErr := os.Stat(fullFilePath)
	if fsErr == nil {
		size = fs.Size()
		lastUpdated = fs.ModTime().Unix()
	}

	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	resp := map[string]interface{}{"md5": md5Sum, "size": size, "last_updated": lastUpdated}
	json.NewEncoder(w).Encode(resp)

}

func setRoutes(r *mux.Router) {
	r.HandleFunc("/", HomeHandler).Methods("GET")
	r.HandleFunc("/goz/stat/{path:.*}", ContentStatHandler).Methods("GET")
	r.HandleFunc("/goz/{path:.*}", ContentPutHandler).Methods("PUT")
	r.HandleFunc("/goz/{path:.*}", ContentDeleteHandler).Methods("DELETE")

}

func main() {

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if os.Getenv("GOZ_DEBUG") != "" {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	r := mux.NewRouter()
	setRoutes(r)

	c := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowCredentials: true,
		AllowedHeaders:   []string{"Authorization", "Content-Type"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE"},
	})
	handler := c.Handler(r)

	loggedRouter := handlers.LoggingHandler(os.Stdout, handler)

	listen := "127.0.0.1"
	port := 8090
	if os.Getenv("GOZ_LISTEN") != "" {
		listen = os.Getenv("GOZ_LISTEN")
	}
	if os.Getenv("GOZ_PORT") != "" {
		port, _ = strconv.Atoi(os.Getenv("GOZ_PORT"))
	}

	srv := &http.Server{
		Handler: loggedRouter,
		Addr:    fmt.Sprintf("%s:%d", listen, port),
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	srv.ListenAndServe()

}
